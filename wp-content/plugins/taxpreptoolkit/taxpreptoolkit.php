<?php
/**
 * Plugin Name: Davies Allen Tax Prep Toolkit
 * Plugin URI: http://anchoralpine.com
 * Description: Tax Prep Toolkit Form
 * Version: 3.4.1
 * Author: Anchor & Alpine, Inc.
 * Author URI: http://anchoralpine.com/
 * Text Domain: dav-taxpreptoolkit
 */
$pluginPath = dirname(__FILE__);
global $TAXYEAR;
require 'plugin-update-checker.php';
require 'helpers.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://anchoralpine.com/taxprepLV9HkytvDHjXq78fdmK9pdmzsmcnbKig/details.json',
	__FILE__,
	'taxpreptoolkit'
);

// Set Plugin URL
$pluginUrl = plugins_url( 'taxpreptoolkit');

function toolkit_options_cb() {
  ?>
<div>
  <h2>TaxPrep ToolKit Options</h2>
    <form method="post" action="options.php">

    <?php settings_fields('taxpreptoolkit-settings'); ?>
 <?php do_settings_sections('taxpreptoolkit'); ?>
  <?php  submit_button(); ?>
  </form>
    
</div>
<?php
}


function toolkit_settings_init() {
  register_setting('taxpreptoolkit-settings', 'taxpreptoolkit_options');
  add_settings_section( 'taxpreptoolkit-settings', 'General Options', 'taxpreptoolkit_cb', 'taxpreptoolkit');
  add_settings_field('taxyear', 'Tax Year', 'taxprep_stringfield_cb', 'taxpreptoolkit', 'taxpreptoolkit-settings', array('name' => 'taxyear'));
  
  add_settings_field('emailfrom', 'Google Account to send with', 'taxprep_stringfield_cb', 'taxpreptoolkit', 'taxpreptoolkit-settings', array('name' => 'emailfrom'));
  add_settings_field('emailpassword', 'Google Account Password', 'taxprep_passfield_cb', 'taxpreptoolkit', 'taxpreptoolkit-settings', array('name' => 'emailpassword'));
  add_settings_field('tempdir', 'Temp Directory', 'taxprep_stringfield_cb', 'taxpreptoolkit', 'taxpreptoolkit-settings', array('name' => 'tempdir'));
  add_settings_field('gpgpath', 'Path to GPG', 'taxprep_stringfield_cb', 'taxpreptoolkit', 'taxpreptoolkit-settings', array('name' => 'gpgpath'));
  add_settings_field('wkhtmlpath', 'Path to wkhtmltopdf', 'taxprep_stringfield_cb', 'taxpreptoolkit', 'taxpreptoolkit-settings', array('name' => 'wkhtmlpath'));

add_settings_field('dropboxtoken', 'Dropbox API Access Token', 'taxprep_passfield_cb', 'taxpreptoolkit', 'taxpreptoolkit-settings', array('name' => 'dropboxtoken'));
}

function toolkit_options_page() {
  add_menu_page( 'Taxprep Toolkit', 'TaxPrep Toolkit', 'manage_options', 'toolkit_options', 'toolkit_options_cb');
}

function taxpreptoolkit_cb() {
  echo "You can set the current tax year and other toolkit settings here.<br>Temp Directory is where the utility stores files. This should be a directory that is not cleared periodically like /tmp can be.<br>Path to GPG is the path to the gpg utility on the system. This is generally /usr/bin/gpg but can vary depenting on distribution.";
}

function taxprep_stringfield_cb($args) {
  
  $settings = (array) get_option('taxpreptoolkit_options');
  $field = $args['name'];
  $value = esc_attr($settings[$field]);
  if($field == 'taxyear' && !$value) {
    $value = date('Y',strtotime('-1 year'));
  } else if($field == 'gpgpath' && !$value) {
    $cmd = exec("which gpg");
    $value = $cmd;
  } else if($field == 'wkhtmlpath' && !$value) {
    $cmd = exec('which wkhtmltopdf');
    $value = $cmd;
  } else if($field == 'tmpdir' && !$value) {
    
  }
  
  echo '<input type="text"  style="width:50%" name="taxpreptoolkit_options['.$field.']" value="'.$value.'" />';
  
}

function taxprep_passfield_cb($args) {
  
  $settings = (array) get_option('taxpreptoolkit_options');
  $field = $args['name'];
  $value = esc_attr($settings[$field]);
  
  echo '<input type="password" style="width:50%" name="taxpreptoolkit_options['.$field.']" value="'.$value.'" />';
  
}
function toolkit_enqueue_scripts() {
  wp_enqueue_script( 'autonumeric', plugins_url( '/js/autoNumeric-min.js', __FILE__ ), array('jquery'));
  wp_enqueue_script( 'maskedinput', plugins_url( '/js/jquery.maskedinput-1.3.min.js', __FILE__ ), array('jquery'));
  wp_enqueue_script( 'scrollto', plugins_url( '/js/jquery.scrollTo-1.4.3.1-min.js', __FILE__ ), array('jquery'));
  wp_enqueue_script( 'simplemodal', plugins_url( '/js/jquery.simplemodal.1.4.2.min.js', __FILE__ ), array('jquery'));
  wp_enqueue_script( 'taxprep', plugins_url( '/js/taxstart.js?v=5.9', __FILE__ ), array('jquery', 'scrollto','maskedinput','autonumeric','simplemodal'));
  wp_enqueue_script( 'taxform', plugins_url( '/js/tax.js?v=5.10', __FILE__ ), array('taxprep'));
  wp_enqueue_style( 'taxstyles', plugins_url( '/taxorganizerstyles.css', __FILE__ ));
}

add_action('admin_init', 'toolkit_settings_init');
add_action('admin_menu', 'toolkit_options_page');
add_action('wp_enqueue_scripts','toolkit_enqueue_scripts');


function taxprep_startToolkit() {
  global $TAXYEAR;
  $TAXYEAR = taxprep_taxyear();
 
  return '<div id="maincontain">
    <div id="intro"><hr />
    <h1>'.$TAXYEAR.' Tax Prep Toolkit</h1>
    <hr />
    <p>Help us find every tax deduction and credit you are eligible for—our tax review tool will help you fully prepare for your meeting with us.</p>
    </div>
      <p>&nbsp;</p>
    <p>When using this Tax Prep Toolkit you are submitting <strong>sensitive information</strong>. Because of this there are a few things to know:</p>
    <div id="taxorganizer">
    <form id="form1" name="form1" method="post" action="">
      <p class="know"><div class="arrow"></div>Once you start to input your information you will have 72 hours to complete it on the same browser/device. Your information is stored on your local machine/device until you click submit and then it is transmitted to Davies + Allen.</p>
      <p class="highlight">
        <input type="checkbox" name="timed" id="timed" />
        <label for="timed">Got it, I\'ll finish within 72 hours and I understand that I need to use the same computer, mobile phone or tablet from start to finish.</label>
  
      </p>
      <p class="know"><div class="arrow"></div>This information should only be transmitted over a trusted wifi connection and NOT a wireless (mobile phone) or public wifi connection. </p>
      <p class="highlight">
        <input type="checkbox" name="device" id="device" />
        <label for="device">Ok, I\'ll only submit on a trusted wifi connection. </label>
  		</p>
      <p class="center"><span id="getinfo">Please acknowledge the time/device constraint and security information above. </span>
        <input type="button" name="submit" disabled="disabled" id="page01" tabindex="-1" value="Get Started ›" class="btn" />
      </p>
    </form></div>
  </div>';

}

add_shortcode('taxpreptoolkit', 'taxprep_startToolkit');
