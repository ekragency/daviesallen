<?php
require_once('classes/class.phpmailer.php');
require_once('classes/class.smtp.php');
require_once( 'helpers.php' );
ini_set('display_errors',false);
error_reporting(E_ERROR);

$tmpdir = taxprep_tmppath(); 
$gpg = taxprep_gpgpath();
$wkhtml = taxprep_getsetting('wkhtmlpath');
$emailfrom = taxprep_getsetting('emailfrom');
$emailpassword = taxprep_getsetting('emailpassword');
$dropboxapi = taxprep_getsetting('dropboxtoken');
$homedir = $tmpdir . "/.gnupg";
$plugindir = taxprep_get_home_path() . 'plugins/taxpreptoolkit/';
$dropbox = $tmpdir . '/.dropbox'; 

$pubkey = "Davies Allen Tax Prep Toolkit";
$taxyear =taxprep_taxyear();

$enduser = array('Wages' => 'W-2', 'StateRefund' => '1099-G', 'Interest' => '1099-INT', 'Land' => 'Land Sales', 'Dividend' => '1099-DIV',
				'Partnership' => 'K1', 'Stock' => '1099-B', 'Rental' => '1099-MISC', 'IRA' => '1099-R', 'Royalty' => '1099-MISC',
				'Commissions' => '1099-MISC', 'Prizes' => '1099-MISC', 'Unemployment' => '1099-G', 'Gambling' => 'W-2G', 'SocialSecurity' => '1099-SSA',
				'Debt' => '1099-C', 'Farming' => '1099-G', 'HSA' => '1099-SA, 5498-SA', 'EIPA' => '1044', 'EIPB' => '1044-B', 'TaxPayerTuition' => '1098-T', 'SpouseTuition' => '1098-T');

if(!$tmpdir || !$gpg || !$wkhtml || !$emailfrom || !$emailpassword || !$dropboxapi) {
	echo json_encode(array('resultCode' => -1, "message" => "Required settings missing. Please check your settings in the admin section."));
	exit;
}

if(!file_exists($tmpdir)) {  
	mkdir($tmpdir); 
	if(!file_exists($tmpdir)) {
		echo json_encode(array('resultCode' => -1, "message" => "Unable to create work directory. Please check your settings in the admin section."));
		exit;
	}
}

$file = $tmpdir . "/dav_tax_".time()."_".getmypid().".html";
$filecsv = $tmpdir . "/dav_tax_".time()."_".getmypid().".csv";
$pdffile = preg_replace('~\.html~','.pdf', $file);

$data = $_POST;
$payload = '';
$outdata = '';

header('Access-Control-Allow-Origin: https://wptestbed.a-a.dev');
if(!is_array($data) || sizeof($data) == 0) {
		echo json_encode(array("resultCode" => -1, "message" => "Form Error."));
		exit;
}

if(!$data['Email2']) {
	echo json_encode(array("resultCode" => -1, "message" => "Email is required."));
	exit;
}

foreach($data as $k => $v) {
	if(is_array($v)) {
		$sub = 1;
		foreach($v as $k2 => $v2) {

			$payload .= '"'.$k . $sub . '","'.preg_replace("/\"/","",$v2).'"'."\n";
			$sub++;
		}
	} else {
		$payload .= '"'.$k.'","'.preg_replace("/\"/","",$v) .'"'."\n";
		if(array_key_exists($k, $enduser) && $v == 'Yes') {
			$enduserforms[] = $enduser[$k] . " (".$k.")";
		}
	}
}

ob_start();

include "buildform.php";
$outform = ob_get_contents();

ob_end_clean();

if($data['Email2']) { $enduserto = $data['Email2']; }

$f = fopen($filecsv, "w+");
if($f || !$payload) {
	fwrite($f, $payload);
	fclose($f);

	// Make sure GPG is still set up correctly
	if(filesize($homedir.'/pubring.gpg') == 0 || filesize($homedir.'/trustdb.gpg') == 0 || !file_exists($homedir.'/gpg.conf') || !file_exists($homedir.'/random_seed')) {
		rename($tmpdir.'/.gnupg',$tmpdir.'/.gnupg2');
	} 
	if(!file_exists($tmpdir) || !file_exists($homedir)) {
		if(!file_exists($tmpdir)) { mkdir($tmpdir); }
		exec($gpg . " --batch --yes --homedir ".$homedir." --import pubkey.pub",$output);
		copy($plugindir . 'trustdb.gpg','/tmp/davtaxform/.gnupg/trustdb.gpg');
	}

	exec($gpg . " --batch --yes --always-trust --homedir ".$homedir." -er '".$pubkey."' ".$filecsv, $output);
	if(file_exists($filecsv.".gpg")) { unlink($filecsv); }
	rename($filecsv.".gpg", $filecsv.".pgp");

	file_put_contents($file, $outform);
	
	exec($wkhtml .' '.$file.' '.$pdffile, $WKOutput);
//	exec('./dropbox_uploader.sh upload '.$file.' '.preg_replace("|/tmp/davtaxform/|","",$file) );
	if(!file_exists($pdffile)) { 
		echo json_encode(array('resultCode' => -1, "message" => "Unable to create PDF File. Please check your settings in the admin section."));
		$fp = fopen($tmpdir . '/PDFLog.log', "w+");
		fwrite($fp, var_export($WKOutput,true));
		fclose($fp);
		exit;
		
	}
	if(file_exists($pdffile)) unlink($file);
	$cheaders = array('Authorization: Bearer '.$dropboxapi,
										'Content-Type: application/octet-stream',
										'Dropbox-API-Arg: {"path":"/'.basename($pdffile).'", "mode":"add"}');
	$ch = curl_init('https://content.dropboxapi.com/2/files/upload');
	$fp = fopen($pdffile, 'rb');
	curl_setopt($ch, CURLOPT_HTTPHEADER, $cheaders);
	curl_setopt($ch, CURLOPT_PUT, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($ch, CURLOPT_INFILE, $fp);
	curl_setopt($ch, CURLOPT_INFILESIZE, filesize($pdffile));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);											
	$response = curl_exec($ch);
	//exec('./dropbox_uploader.sh -f '.$tmpdir.'/.dropbox upload '.$pdffile.' '.basename($pdffile) );
	fclose($fp);
//	echo $response;
//	$lf = fopen('/tmp/davtaxform/log', 'w');

//	fwrite($lf, $lout);
//	fclose($lf);

	$tst = exec($gpg . " --batch --yes --always-trust --homedir ".$homedir." -er '".$pubkey."' ".$pdffile, $output, $resultcode);
	if(file_exists($pdffile.".gpg")) { unlink($pdffile); }
	else {
		echo json_encode(array('resultCode' => -1, "message" => "Unable to encrypt PDF. (".$resultcode." | ".$tst.") Please check your settings in the admin section.<br>".var_export($output,true)));
		exit;

	}
	rename($pdffile.".gpg", $pdffile.".pgp");


	$to = 'wendy@daviesallen.com';
	$sendermail = $emailfrom;// 'files@daviesallen.com';

 	multi_attach_mail($to, basename($pdffile), $sendermail);

//	unlink($file.".pgp");

	$returnpath = "-f".$sendermail;
    $fromname = "Davies + Allen";
    $from =  $sendermail;
	$headers = "Content-type: text/html; charset=UTF-8\r\nFrom: ".$from;

	/* Insert HTML message */

	$endusermessage = '<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Davies + Allen Tax Prep Toolkit</title>
</head>
<body>
<p style="text-align:center"><img src="http://daviesallen.com/wp-content/plugins/taxpreptoolkit/taxorg-images/davies_allen_logo_email.png" width="345" height="104" alt="Davies + Allen" /></p>
<div style="font-family:Arial, Helvetica, sans-serif; color:#333; margin:20px;"><p>Hello '.$data['FirstName'].',</p>
<p>Thank you for submitting your information through the <strong>'.$taxyear.' Tax Prep Toolkit</strong>. Davies + Allen has received a copy of your information and our team will contact you soon. </p>';
	if(count($enduserforms)) {
		$endusermessage.='<p>In the meantime, you marked that you have the following documents, please gather them together now and submit them to us via ShareFile, email, fax, in person drop-off or snail mail::</p>
<ul style="margin:40px; border:1px solid #CCC; background:#EDEDED; padding:40px;">';
		foreach($enduserforms as $v) {
			$endusermessage.= "<li>" . $v . "</li>\n";
		}
		$endusermessage .= "</ul>";
	}
	$endusermessage .= '<p>A friendly reminder that you need written documentation supporting the claims to all tax deductions. If you are one of the lucky ones selected by the IRS for audit they could ask you to produce these written documents supporting your tax positions. Some of these deductions include business vehicle use, business entertainment, home computer use, charitable contributions and others.</p>
<p><strong><em>Thank you for allowing Davies + Allen the opportunity to serve you.</em></strong></p>
<hr />
<table width="100%" border="0" cellspacing="0" cellpadding="10" style="font-family:Georgia, "Times New Roman", Times, serif;">
  <tr><td colspan="3" bgcolor="#242424"><p style="color:#FFF; font-family:Arial, Helvetica, sans-serif">If you have any questions please contact us at: <a href="http://www.daviesallen.com" style="color:#CCC;">www.daviesallen.com</a></p></td></tr>
  <tr><td width="33%" bgcolor="#D6D2C6"><strong>Orem</strong><br />550 East 770 North / Orem, UT 84097<br />
<em>t</em> 801.225.5854<br />
<em>f</em> 801.783.4892</td>
    <td width="33%" bgcolor="#D6D2C6"><strong>Heber </strong><br />
90 West 200 South, Ste. 3 / Heber, UT 84032<br />
<em>t</em> 435.654.1724<br />
<em>f</em> 801.783.4892</td>
    <td bgcolor="#D6D2C6"><strong>Salt Lake City</strong><br />
1935 E. Vine Street Suite 200 / Salt Lake City, UT 84121<br />
<em>t</em> 801.930.9401<br />
<em>f</em> 801.783.4892</td>
</td>
  </tr>
</table>
</div>
</body>
</html>';

$endusertextmessage = 'Hello '.$data['FirstName'].',

Thank you for submitting your information through the '.$taxyear.' Tax Prep Toolkit. Davies + Allen has received a copy of your information and our team will contact you soon.

';
	if(count($enduserforms)) {
		$endusertextmessage.='In the meantime, you marked that you have the following documents, please gather them together now and submit them to us via ShareFile, email, fax, in person drop-off or mail:

';
		foreach($enduserforms as $v) {
			$endusertextmessage.= "\t".$v."\n";
		}
		$endusertextmessage .= "\n";
	}
$endusertextmessage .= 'A friendly reminder that you need written documentation supporting the claims to all tax deductions. If you are one of the lucky ones selected by the IRS for audit they could ask you to produce these written documents supporting your tax positions. Some of these deductions include business vehicle use, business entertainment, home computer use, charitable contributions and others.

Thank you for allowing Davies + Allen the opportunity to serve you.

If you have any questions please contact us at: www.daviesallen.com

Orem
550 East 770 North / Orem, UT 84097
t 801.225.5854 / f 801.783.4892

Heber
90 West 200 South, Ste. 3 / Heber, UT 84032
t 435.654.1724 / f 801.783.4892

Salt Lake City
1935 E. Vine Street Suite 200 / Salt Lake City, UT 84121
t 801.930.9401 / f 801.783.4892';

	$res = multipart_email($enduserto, "Davies + Allen Tax Prep Toolkit",$from, $fromname, $returnpath, $endusermessage, $endusertextmessage);

	echo json_encode(array("resultCode" => 0, "message" => "Submit Success", "data" => $outdata));

} else {
	echo json_encode(array("resultCode" => -2, "message" => "Unable to create form file"));
}


//Serious perhaps confusion mazza? 1933.

function multi_attach_mail($to, $files, $sendermail){
	global $data;

    // email fields: to, from, subject, and so on
    $from = $sendermail;
    $fromname = "Tax Form Submission";
    $returnpath = $from;
    $subject = "Davies + Allen Tax Prep Toolkit Submission";
    $message = '<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Davies Allen Tax Prep Toolkit</title>
</head>

<body>
<div style="font-family:Arial, Helvetica, sans-serif; color:#333; margin:20px;">
  <p>There is a new <strong>'.$taxyear.' Tax Prep Toolkit</strong> submission in Dropbox.</p>
  <table width="100%" border="0" cellspacing="10" cellpadding="0" bgcolor="#EDEDED">
    <tr>
      <td width="13%">Taxpayer:</td>
      <td width="87%"><strong>'.$data['FirstName'].' '.$data['LastName'].'</strong></td>
    </tr>
    <tr>
      <td style="white-space: nowrap;">Home Phone:</td>
      <td><strong>'.$data['HomePhone'].'</strong></td>
    </tr>
    <tr>
      <td style="white-space: nowrap;">Mobile Phone:</td>
      <td><strong>'.$data['MobilePhone'].'</strong></td>
    </tr>
    <tr>
      <td style="white-space: nowrap;">Email Address:</td>
      <td><strong>'.$data['Email2'].'</strong></td>
    </tr>
  </table>
  <p>Please complete the following tasks:</p>
  <ol>
    <li>Access your dropbox folder and look in the "/Apps/DAV Tax Prep Toolkit" folder for the file '.$files.'</li>
    <li>Contact the client with further instructions (client has been sent a confirmation email with a list of items to gather).</li>
    <li>If client has opted in to Audit Insurance please notify Brad or JW.</li>
  </ol>
</div>
</body>
</html>';

	$messagetxt = 'There is a new '.$taxyear.' Tax Prep Toolkit Submission in Dropbox.

	Taxpayer: '.$data['FirstName'].' '.$data['LastName'].'
	Home Phone: '.$data['HomePhone'].'
	Mobile Phone: '.$data['MobilePhone'].'
    Business Phone: '.$data['BusinessPhone'].'
    Email Address: '.$data['Email2'].'
    Business Name: '.$data['BusinessName'].'

Please complete the following tasks:
	- Access your dropbox folder and look in the "/Apps/TaxPrepSiteUploads" folder for the file '.$files.'
    - Contact the client with further instructions (client has been sent a confirmation email with a list of items to gather).
    - If client has opted in to Audit Insurance please notify Brad or JW.';
    $ok = multipart_email($to, $subject, $from, $fromname, $returnpath, $message, $messagetxt);
    return($ok);
}


function multipart_email($_emailer_to,$_emailer_subject,$_emailer_from,$_emailer_from_name, $_emailer_reply_to,$htmlbody,$textbody) {
	global $emailfrom;
	global $emailpassword;
	$mail = new PHPMailer(true); //defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch
	try {
		$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;
		$mail->isSMTP();
       // Enable SMTP authentication
		$mail->Username = $emailfrom;                 // SMTP username
		$mail->Password = $emailpassword;                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$mail->setFrom($_emailer_from, $_emailer_from_name);
		$mail->addAddress($_emailer_to);
		$mail->addReplyTo('wendy@daviesallen.com', 'Wendy Dalton');
		$mail->Subject = $_emailer_subject;
		$mail->Body    = $htmlbody;
		$mail->AltBody = $textbody;
		$result = $mail->send();
		return $result;
	} catch (phpmailerException $e) {
echo json_encode(array("resultCode" => -2, "message" => "Unable to email " . $emailfrom. ' <> ' . $emailpassword . ' <> ' . $e->errorMessage));
exit; 		
		print_r($e);
		return $e->errorMessage;
	} catch (Exception $e) {
		return false;	//  return('xx');
	}
	return false;


}
