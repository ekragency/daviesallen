    <h1>Estimated Tax Payments</h1>
<?php include "include-complete.php"; ?>
      <div class="formcontain">
 <p class="yesnosection">
        <label for="EstimatedPayments" class="label400">Did you make estimated tax payments this year?</label>
        <span class="answer"><label class="rlabel"><input type="radio" name="EstimatedPayments" class="p4f" id="EstimatedPayments" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="EstimatedPayments" id="EstimatedPayments"  class="p4f" value="no" /> no </label>     </span></p>
     
      <div id="EstimatedPaymentsForm"  style="display:none;">
      <p>
        <label for="PaymentDate1">Statutory Date</label>
        <input name="PaymentDate1" type="text" id="PaymentDate1" class="p4f"  value="7/15/<?php echo $TAXYEAR; ?>"/>
      </p>
      <p>
        <label for="PaymentAmount1">Payment Amount</label>
        <input type="text" name="PaymentAmount1" id="PaymentAmount1" class="p4f dollar" /><br /><br /><br /><br />
      </p>
      <p>
        <label for="PaymentDate2">Statutory Date</label>
        <input name="PaymentDate2" type="text" id="PaymentDate2" class="p4f"  value="7/15/<?php echo $TAXYEAR; ?>"/>
      </p>
      <p>
        <label for="PaymentAmount2">Payment Amount</label>
        <input type="text" name="PaymentAmount2" id="PaymentAmount2" class="p4f dollar" ><br /><br /><br /><br />
      </p>
      <p>
        <label for="PaymentDate3">Statutory Date</label>
        <input name="PaymentDate3" type="text" id="PaymentDate3" class="p4f"  value="9/15/<?php echo $TAXYEAR; ?>"/>
      </p>
      <p>
        <label for="PaymentAmount3">Payment Amount</label>
        <input type="text" name="PaymentAmount3" id="PaymentAmount3" class="p4f dollar"  /><br /><br /><br /><br />
      </p>
      <p>
        <label for="PaymentDate4">Statutory Date</label>
        <input name="PaymentDate4" type="text" id="PaymentDate4" class="p4f"  value="1/15/<?php echo $TAXYEAR+1; ?>"/>
      </p>
      <p>
        <label for="PaymentAmount4">Payment Amount</label>
        <input type="text" name="PaymentAmount4" id="PaymentAmount4" class="p4f dollar"  /></p>
      </div>
      </div><!--formcontain-->
<p class="btnposition">
      <input type="button" name="submit" id="page04" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
