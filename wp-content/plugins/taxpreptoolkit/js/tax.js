var MAXIDS = 14;
var opencount = 0;
var curpage = 0;
var maxpage = 14;
var submitted = 0;
var dep = '<p class="addsubtract depremove"><a href="#"><img src="/wp-content/plugins/taxpreptoolkit/taxorg-images/icon-circle-minus.png" width="30" height="30" alt="Add" /> Remove Dependent</a></p><p><label for="DependentName">Name</label><input class="p3f"  type="text" name="DependentName[]" id="DependentName%CT%" style="width:300px;"/></p><p><label for="DependentSSN">SSN</label><input class="p3f"  name="DependentSSN[]" type="text" id="DependentSSN%CT%" style="width:300px;color:#aaa;" def="### - ## - ####" value="### - ## - ####"/></p><p><label for="DependentBirthday">Birthday</label><input class="p3f"  type="text" name="DependentBirthday[]" id="DependentBirthday%CT%" style="width:300px;"/></p><p class="noline"><label for="DependentDisabled" class="radiolabel">Disabled</label><label class="rlabel"><input class="p3f"  type="radio" name="DependentDisabled%CT%" id="DependentDisabled%CT%" value="yes" /> yes</label> &nbsp;&nbsp;<label class="rlabel"><input class="p3f"  type="radio" name="DependentDisabled%CT%" id="DependentDisabled%CT%" value="no" /> no</label> </p><h2>Dependent College Student</h2><p><label for="DependentCollege%CT%" class="radiolabel">College or Trade School</label><label class="rlabel"><input class="p3f"  type="radio" name="DependentCollege%CT%" id="DependentCollege%CT%" value="yes" /> yes &nbsp;&nbsp;<label class="rlabel"><input class="p3f"  type="radio" name="DependentCollege%CT%" id="DependentCollege%CT%" value="no" /> no</label> </p><p><label for="DependentHalftime" class="radiolabel">At Least 1/2 Time Student</label><label class="rlabel"><input class="p3f"  type="radio" name="DependentHalftime%CT%" id="DependentHalftime%CT%" value="yes" /> yes &nbsp;&nbsp;<label class="rlabel"><input class="p3f"  type="radio" name="DependentHalftime%CT%" id="DependentHalftime%CT%" value="no" /> no</label></p><p><label for="DependentYear" class="radiolabel">Year</label><label class="rlabel"><input class="p3f"  type="radio" name="DependentYear%CT%" id="DependentYear%CT%" value="Freshman" />Freshman &nbsp;&nbsp;</label> <label class="rlabel"><input class="p3f"  type="radio" name="DependentYear%CT%" id="DependentYear%CT%" value="Sophmore" />Sophmore &nbsp;&nbsp;</label> <label class="rlabel"><input class="p3f"  type="radio" name="DependentYear%CT%" id="DependentYear%CT%" value="Junior" /> Junior</label><label class="rlabel"> &nbsp;&nbsp;<input class="p3f"  type="radio" name="DependentYear%CT%" id="DependentYear%CT%" value="Senior" /> Senior </label></p><p><label for="DependentTuition%CT%" class="radiolabel">Have Tuition Form 1098-T <input class="p3f"  type="checkbox" name="DependentTuition%CT%" id="DependentTuition%CT%" /></label></p>';
var depaddsb = '<p class="addsubtract depadd"><a href="#"><img src="/wp-content/plugins/taxpreptoolkit/taxorg-images/icon-circle-plus.png" width="30" height="30" alt="Add" /> Add Another Dependent</a></p>';
var org = '<p><label for="Organization">Organization</label><input type="text" name="Organization%TYPE%[]" class="p12f" id="Organization%CT%" style="width:300px;"/></p><p><label for="OrganizationAmount">Amount</label><input name="OrganizationAmount%TYPE%[]" type="text" id="OrganizationAmount%CT%" class="p12f dollar" style="width:300px;"/></p>';
var reqfields = {'FirstName':'First Name','LastName':'Last Name','Email2':'Email Address'};
var reqchecks = {'Agree':'You must agree to the terms.','Understand':'You understand that by clicking Submit that your information will be transmitted to Cook Martin Poulson.','Transmit':'You understand that you should only transmit this information over a trusted wifi connection and NOT over a wireless or public wifi connection.'};



//  Basic open/close functionality
function accordian(e) {
	var id='#c'+jQuery(this).attr('id').replace(/p/,'');
	var inhtml = jQuery(this).html();

	if(jQuery(id).is(':visible')) {
		jQuery(this).html(inhtml.replace('▾','▸'));
		jQuery(id).slideUp(400);
		opencount--;
	} else {
		jQuery(this).html(inhtml.replace('▸','▾'));
		jQuery(id).slideDown(400);
		opencount++;
	}
	checkopen();

	e.preventDefault();
}

// Used for auto-closing/opening
function closesection(i) {
	var id='#c'+i;
	var pid='#p'+i;
	var inhtml = jQuery('#p'+i).html();
	
	if(jQuery(id).is(':visible')) {
		jQuery(pid).html(inhtml.replace('▾','▸'));
		jQuery(id).slideUp(400);
		opencount--;
	}
	checkopen();
}
function fastclosesection(i) {
	var id='#c'+i;
	var pid='#p'+i;
	var inhtml = jQuery('#p'+i).html();
	
	if(jQuery(id).is(':visible')) {
		jQuery(pid).html(inhtml.replace('▾','▸'));
		jQuery(id).hide();
		opencount--;
	}
	checkopen();
}
function opensection(i,st) {
	var id='#c'+i;
	var pid='#p'+i;
	var inhtml = jQuery('#p'+i).html();
	
	if(!jQuery(id).is(':visible')) {
		jQuery(pid).html(inhtml.replace('▸','▾'));
		jQuery(id).slideDown(400);
		opencount++;
	}
	checkopen();
	
}
function fastcloseall() {
	for(i=0;i<=MAXIDS;i++) {
		fastclosesection(i);
	}

	jQuery('#closeall').hide();
	jQuery('#openall').show();
	
	
}
// Closes/opens all sections
function closeall(e) {
	for(i=0;i<=MAXIDS;i++) {
		closesection(i);
	}

	jQuery('#closeall').hide();
	jQuery('#openall').show();

	e.preventDefault();
}
function openall(e) {
	for(i=0;i<=MAXIDS;i++) {
		opensection(i);
	}
	
	jQuery('#openall').hide();
	jQuery('#closeall').show();

	e.preventDefault();
}

// Closes current and if needed opens the next section.
function continuenext(e) {
	var i=jQuery(this).parent().parent().attr('id').replace(/c/,'');

	closesection(i);
	setTimeout(function() {	jQuery(document).scrollTo('#p'+i,400); }, 200);

	var nexti = parseInt(i)+1;

	if(nexti <= MAXIDS) {
		opensection(nexti,1);		
		jQuery(document).scrollTo('#p'+nexti);
		//setTimeout(function() { jQuery(document).scrollTo('#p'+nexti,400); }, 600);

	}

	e.preventDefault();
}

// Handles display of open all / close all
function checkopen(){
	if(!opencount){
		jQuery('#closeall').hide();
		jQuery('#openall').show();
	} else {
		jQuery('#openall').hide();
		jQuery('#closeall').show();
	}
}

function scanform($elem) {
	if(!$elem.jquery) $elem = jQuery(this);
	var scanclass = $elem.attr('class').replace(/\s*dollar\s*/,'');
	var parentid = '#'+scanclass.replace(/f/,''); 
	var formid = parentid.replace(/p/,'c');
	var checkid = '#checkbox'+scanclass.replace(/[a-z]/g,'');
	var nogo = 0;
	
	jQuery('.'+scanclass).each(function() {
	 	var $subelem = jQuery(this);

		var curcolor = $subelem.css('color');
		var def = $subelem.attr('def');
		if(curcolor != "rgb(170, 170, 170)") {
			if($subelem.val()){
				jQuery(parentid).find('.statcircle').removeClass('status-clear').removeClass('status-checked').addClass('status-question');
				jQuery(checkid).prop('checked', false);
			} else {
				if(def) $subelem.val(def).css('color','#aaa');
			}
		} else {
			if(def != $subelem.val()) {
				jQuery(this).css('color','#000');
				if(!$subelem.val()) $subelem.val(def).css('color','#AAA');
			}
		}
		
		if($subelem.attr('type') == 'checkbox') {
			if(!$subelem.is(':checked')) nogo = 1;
		} else if($subelem.attr('type') == 'radio') {
			if(!jQuery('#'+$subelem.attr('id')+":checked").val()) nogo = 1;
		} else {	
			if((!$subelem.val() || $subelem.val() == def)) {
				nogo = 1;
			} else {
				jQuery(parentid).find('.statcircle').removeClass('status-clear').removeClass('status-checked').addClass('status-question');
			}
		}
	});

	if(!nogo) {
		jQuery(parentid).find('.statcircle').removeClass('status-clear').removeClass('status-question').addClass('status-checked');
		jQuery(formid).find('.completecheck').attr('checked','checked');
	}
}
function defaultscan() {
	var $elem = jQuery(this);
	scanform(jQuery(this));
	var def = $elem.attr('def');
	var curcolor = $elem.css('color');
	if(def && curcolor == 'rgb(170, 170, 170)') {
		$elem.css('color','#000');
		$elem.val('');
	}
}
function checkform() {
	var scanclass = jQuery(this).parent().parent().attr('id').replace(/[a-z]/g,'');
	var parentid = '#p'+scanclass;
	var state = jQuery(this).attr('checked');

	if(!state) {
		jQuery(parentid).find('.statcircle').removeClass('status-clear').removeClass('status-checked').addClass('status-question');
	
	} else {
		jQuery(parentid).find('.statcircle').removeClass('status-clear').removeClass('status-question').addClass('status-checked');
	}
	
}

function mbutton() {
	var button = jQuery(this);
	var parentform = jQuery(this).parent().parent();
	var checkbox = jQuery(this).find('input[type="checkbox"]');	
	var global = 0;

	if(checkbox.hasClass("MonthsAll")) {
		global = 1;
	}
	
	
	if(checkbox.is(':checked')) {
		if(global) {
			parentform.find('td.mbutton').removeClass('mbuttonactive');
			parentform.find('td.mbutton input').prop('checked',false);
		} else {
			checkbox.prop('checked',false);
			button.removeClass('mbuttonactive');
// 			parentform.find('input[name="TaxPayerMonthsAll"]').parent().removeClass('mbuttonactive');
		}
	} else {
		if(global) {
			parentform.find('td.mbutton').addClass('mbuttonactive');
			parentform.find('td.mbutton input').prop('checked',true);
		} else {
			checkbox.prop('checked', true);
/*
			parentform.find('input[name="TaxPayerMonthsAll"]').parent().removeClass('mbuttonactive');
			parentform.find('input[name="TaxPayerMonthsAll"]').prop('checked',true);
*/
			button.addClass('mbuttonactive');
		}
	}

	
}

function depadd() {
	var cursize = parseInt(jQuery('.depform').length);
	var content = jQuery('<div></div>').addClass('formcontain').addClass("depform");
	var newdep = dep.replace(/%CT%/g,(cursize+1));
	content.html(newdep).hide();
	content.insertBefore('#p3submit');
	content.fadeIn();
	jQuery('#DependentSSN'+(cursize+1)).mask('999 - 99 - 9999', { placeholder: '#' });
	jQuery('#DependentBirthday'+(cursize+1)).mask('99 / 99 / 9999', { placeholder: '#' });
	
	if(cursize == 0) {
		jQuery('#p3submit').prepend(depaddsb);
	}
	return false;
}

function depremove() {
	jQuery(this).parent().fadeOut(function() { jQuery(this).remove(); if(jQuery('.depform').length == 0) { jQuery('#p3submit').find('.depadd').remove(); } });
	return false;
}

function orgadd() {
    var pid = jQuery(this).parent().attr('id');
    var orgtype = jQuery(this).attr('data');
    var dstclass = 'orgform'+orgtype;
    var dstid = parseInt(jQuery("."+dstclass).length)+1;
	var content = jQuery('<div></div>').addClass(dstclass).attr('id','orgform'+orgtype+dstid);
	var org2 = org.replace(/%TYPE%/g,orgtype);
	org2 = org2.replace(/%CT%/g,dstid);
	content.html(org2).hide();
	content.insertBefore('#'+pid).find('.dollar').autoNumeric({aSign: '$'});
	//autonum = new AutoNumeric('#orgform'+orgtype+dstid+' .dollar',{currencySymbol: '$'});//autonum.update();
	content.fadeIn();
	if(jQuery("."+dstclass).length == 1) {
		jQuery('#'+pid).find('span').html('Add/Remove Organization');
		jQuery('#'+pid).append(' <a class="orgremove" href="#" data="'+orgtype+'"><img src="/wp-content/plugins/taxpreptoolkit/taxorg-images/icon-circle-minus.png" width="30" height="30" alt="Add" /></a>');
	} 
	return false;
}

function orgremove() {
    var pid = jQuery(this).parent().attr('id');
    var orgtype = jQuery(this).attr('data');
    var cursize = parseInt(jQuery(".orgform"+orgtype).length);
    var dstid = 'orgform'+orgtype+cursize;
	jQuery('#'+dstid ).remove();
	if(cursize == 1) {
		jQuery('#'+pid).find('span').html('Add Organization');
		jQuery('#'+pid).find('.orgremove').remove();
		
	}
	return false;
}

function showhide() {
	var $selectorid = jQuery(this);
	var formid = $selectorid.attr('id') + 'Form';
	if($selectorid.val() == 'yes') jQuery('#'+formid).fadeIn();
	else jQuery('#'+formid).fadeOut();
}

/* Page navigation via menu */
function menunav() {
	jQuery.modal.close();
	var page = parseInt(jQuery(this).attr('id').replace(/[a-z]/g,'')); // parseInt(jQuery(this).find('a').attr('href'));
	if(page != undefined) {
		opensection(page,1);
		jQuery(document).scrollTo('#p'+page);
	}
	return false;
}


function formgo(e) {
// 	var sections = 0;
// 	var seclist = '';
// 	jQuery('.completecheck').each(function() { 
// 		var id = jQuery(this).parent().parent().attr('id');
// 
// 		if(!jQuery(this).is(':checked')) {
// 			sections++;
// 			var cursec = id.replace(/[a-z]/g,'');
// 			seclist += "<li id='ep"+cursec+"' href='#'>"+jQuery('#p'+cursec).html().replace(/<br[^>]*>/,'').replace(/[▾▸]+\s*/,'')+"</li>";
// 		}
// 	});
	
	// if(sections) { 
	// 	jQuery.modal("<div id='simplemodal-container'>You have "+sections+" sections that are not marked as being complete.<br/>Click a section name to return to that section.<br/><ul id='errormenu'>"+seclist+"</ul></div>",
	// 				 {onOpen: function (dialog) {dialog.overlay.fadeIn('fast', function () {	dialog.data.hide();	dialog.container.fadeIn('fast', function () {dialog.data.slideDown('fast');	}); });	},
	// 				overlayClose:true
	// 			});
	// 	jQuery('#errormenu li').on('click',menunav).css('cursor','pointer').css('text-decoration','underline');
	// }  else {
		if(submitted == 1) return;
		else submited = 1;
		var missing = '';
		for(i in reqfields) {
			var f = reqfields[i];
			if(!jQuery('#'+i).val()) {
				var cursec = jQuery('#'+i).attr('class').replace(/[a-z]/g,'');
				missing += "<li id='ep"+cursec+"'>"+f+" ("+jQuery('#p'+cursec).html().replace(/<br[^>]*>/,'').replace(/[▾▸]+\s*/,'')+")</li>";
			}
		}
//		console.log(jQuery('#Email2').val());
		if(!validateEmail(jQuery('#Email2').val())) missing += "<li id='ep1'>Email Address is not valid.</li>";


		if(missing) {
			jQuery.modal("<div id='simplemodal-container'>The following fields are required.<br/>Click a field name to return to that section.<br/><ul id='errormenu'>"+missing+"</ul></div>",
					 {onOpen: function (dialog) {dialog.overlay.fadeIn('fast', function () {	dialog.data.hide();	dialog.container.fadeIn('fast', function () {dialog.data.slideDown('fast');	}); });	},
					overlayClose:true
				});
			jQuery('#errormenu li').on('click',menunav).css('cursor','pointer').css('text-decoration','underline');
		} else {
			jQuery.modal("<div id='simplemodal-container'>Submitting data to Davies Allen.</div>",
					 {onOpen: function (dialog) {dialog.overlay.fadeIn('fast', function () {	dialog.data.hide();	dialog.container.fadeIn('fast', function () {dialog.data.slideDown('fast');	}); });	},
					overlayClose:true
				});

			jQuery.post('/wp-content/plugins/taxpreptoolkit/davtaxsub.php',jQuery('#taxform').serialize(), function(data) { 
				jQuery.modal.close();
				if(data.resultCode != 0) {
					jQuery.modal("<div id='simplemodal-container'>There was an unexpected error with the form submission. Please try again later. The error was: <br/>"+data.message+"	</div>",
						 {onOpen: function (dialog) {dialog.overlay.fadeIn('fast', function () {	dialog.data.hide();	dialog.container.fadeIn('fast', function () {dialog.data.slideDown('fast');	}); });	},
						overlayClose:true
					});
				} else {
					jQuery.modal("<div id='simplemodal-container'>Thank you! Your information has been submitted to Davies Allen.</div>",
						 {onOpen: function (dialog) {dialog.overlay.fadeIn('fast', function () {	dialog.data.hide();	dialog.container.fadeIn('fast', function () {dialog.data.slideDown('fast');	}); });	},
						overlayClose:true
					});

					jQuery('#page15').remove();
					jQuery('#taxform').submit(function () { return false; });
					jQuery('#taxmenu li').unbind('click');
					jQuery('.sbutton').unbind('click');

				}
			},'json');

		}
	
	e.preventDefault();
}

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

jQuery(function() {

	
});
