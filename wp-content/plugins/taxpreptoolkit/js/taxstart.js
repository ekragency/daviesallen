/*
$.ctrl = function(key, callback, args) {
    var isCtrl = false;
    $(document).keydown(function(e) {
        if(!args) args=[]; // IE barks when args is null
        
        if(e.ctrlKey) isCtrl = true;
        if(e.keyCode == key.charCodeAt(0) && isCtrl) {
            callback.apply(this, args);
            return false;
        }
    }).keyup(function(e) {
        if(e.ctrlKey) isCtrl = false;
    });        
};
*/

function activatestarted(e) {
	if(jQuery('#timed').is(':checked') && jQuery('#device').is(':checked')) {
		jQuery('#page01').prop('disabled', false).css('background','#666').css('border-color','rgb(102,102,102)').css('color','#fff');
		jQuery('#getinfo').html('Ready to get your information together?');
	} else {
		jQuery('#page01').prop('disabled',true).css('background','#ededed').css('border-color','#ccc').css('color','#ccc');
		jQuery('#getinfo').html('Please acknowledge the time/device constraint and security information above. ');
	}
}

var starttime;
var countdown;
var autonum;

function countdownclock() {
	var current = new Date();
		
	var difference = 72*60*60 - (current - starttime)/1000;
	difference = difference/60/60;
	var hours = Math.floor(difference);
	var minutes = Math.floor((difference-hours)*60);
	var hourtext = "hours";
	var minutetext = "minutes";

	if(hours == 1) hourtext = "hour";
	if(minutes == 1) minutetext = "minute";

	jQuery('#countdown').html(hours + " "+hourtext+ " and " + minutes + " " + minutetext);
	if(hours <10) jQuery('#countdown').addClass('lessthan10');
	if(hours < 0) window.location('/taxpreptoolkit');
}

function getstarted(e) {
//  Ajax Load nextpage.php.
//	window.location = 'nextpage.php'; 
	jQuery.get('/wp-content/plugins/taxpreptoolkit/nextpage.php', function(data) {
		jQuery('#maincontain').html(data);
		jQuery(document).scrollTo('#header');
		jQuery('#SSN, #AlimonyFromSSN, #AlimonySSN').mask('999 - 99 - 9999', { placeholder: '#' });
		jQuery('#HomePhone').mask('( 999 ) 999 - 9999', { placeholder: '#' });
		jQuery('#MobilePhone').mask('( 999 ) 999 - 9999', { placeholder: '#' });
		jQuery('#BusinessPhone').mask('( 999 ) 999 - 9999', { placeholder: '#' });
		jQuery('#SpouseSSN').mask('999 - 99 - 9999', { placeholder: '#' });
		jQuery('#SpouseBirthday, #Birthdate').mask('99 / 99 / 9999', { placeholder: '#' });
		//jQuery('.dollar').autoNumeric({currencySymbol: '$'});
		jQuery('.dollar').autoNumeric({aSign: '$'})
		jQuery('#taxform').on('submit',formgo);
		jQuery('#taxmenu li').on('click',accordian);
		jQuery('#closeall').on('click',closeall);
		jQuery('#openall').on('click',openall);
		jQuery('.sbutton').on('click',continuenext);
		jQuery('.p0f, .p1f,.p2f,.p3f,.p4f,.p5f,.p6f,.p7f,.p8f,.p9f,.p10f,.p11f,.p12f,.p13f,.p14f,.p15f').on('change',scanform);
		jQuery('.p0f, .p1f,.p2f,.p3f,.p4f,.p5f,.p6f,.p7f,.p8f,.p9f,.p10f,.p11f,.p12f,.p13f,.p14f,.p15f').on('focus',defaultscan);
		jQuery('.p0f, .p1f,.p2f,.p3f,.p4f,.p5f,.p6f,.p7f,.p8f,.p9f,.p10f,.p11f,.p12f,.p13f,.p14f,.p15f').on('blur',scanform);
		jQuery('.completecheck').on('change',checkform);
		jQuery('#maincontain').on('click','.depadd',depadd);
		jQuery('#c3').on('click', '.depremove',depremove);
		jQuery('#EstimatedPayments, #IRAContributions, #ChildcarePayments, #AlimonyPayments, #AlimonyReceipt, #DirectDeposit, #ElectronicPayment, #EIPA, #EIPB').on('change',showhide);
	// Organizations
		jQuery('.orgadd').on('click',orgadd);
		jQuery('.addsubtract').on('click','.orgremove',orgremove);
		jQuery('.monthtable td.mbutton').on('click',mbutton);
 		starttime = new Date();
		 fastcloseall();
		 opensection(0);
		countdown = setInterval(countdownclock,6000);
	});
	
	e.preventDefault();
}

jQuery(function() {
	jQuery('#page01').prop('disabled',true).css('background','#ededed').css('border-color','#ccc').css('color','#ccc');
	jQuery('#timed, #device').on('change',activatestarted);
	jQuery('#page01').on('click',getstarted);

/*
	jQuery.ctrl('A', function() {
		jQuery('#p0, #c0, #p3a').show();
	});
*/
});