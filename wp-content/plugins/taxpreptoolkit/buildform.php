<?php 
require_once('helpers.php');
		$monthslist = array(1=> 'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec');
		$TAXYEAR = taxprep_taxyear();
    $plugindir = taxprep_get_home_url() . '/wp-content/plugins/taxpreptoolkit/';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Davies + Allen Tax Organizer Display</title>
<link href="<?php echo $plugindir; ?>/dav-styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="maincontain">
<div class="header">
<div class="logo"><img src="<?php echo $plugindir; ?>/images/davies-allen-logo.png" width="380" height="48" alt="Davies + Allen" /></div>
<div class="title">Tax Organizer <span class="answer">Answers</span>
</div>
<div class="submitted"><p>Organizer submitted: <span class="answer"><?php echo date("m/d/Y"); ?></span></p></div>

</div><!--/header-->
<h1 class="first">Miscellaneous Questions</h1>
<div class="twocol">
	<div class="col">
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>Did the client's marital status change this year?</td>
		<td class="answer"><?php echo $data['MaritalStatusChange']; ?></td>
	</tr>
	<tr>
		<td>Did the client's address change during the year?</td>
		<td class="answer"><?php echo $data['AddressChange']; ?></td>
	</tr>
	<tr>
		<td>Could the client be claimed as a dependent on another person's tax return for the year?</td>
		<td class="answer"><?php echo $data['DependentStatusChange']; ?></td>
	</tr>
	<tr>
		<td>Were there any changes in dependents?</td>
		<td class="answer"><?php echo $data['DependentsChange']; ?></td>
	</tr>
	<tr>
		<td>Were any of the client's unmarried children who might be claimed as dependents 19 years of age or older at the end of the year?</td>
		<td class="answer"><?php echo $data['UnmarriedChildrenChange']; ?></td>
	</tr>
	<tr>
		<td>Did the client have any children under age 18 at the end of 2020 or full-time students under age 24 at the end of 2020, with more than $2100 of unearned income?</td>
		<td class="answer"><?php echo $data['DependentIncomeChange']; ?></td>
	</tr>
  <tr>
    <td>Did the client receive an amount from the first stimulus rebate?</td>
    <td class="answer"><?php echo $data['EIPA']=='no'?'no':$data['EIPAval']; ?></td>
  </tr>
<tr>
  <td>Did the client receive an amount from the second stimulus rebate?</td>
  <td class="answer"><?php echo $data['EIPB']=='no'?'no':$data['EIPBval']; ?></td>
</tr>
</table>
	</div>
	<div class="col">
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>Did the client and the client's dependents have healthcare coverage for the full year?</td>
		<td class="answer"><?php echo $data['HealthCoverageChange']; ?></td>
	</tr>

	<tr>
		<td>Received Form 1905-A</td>
		<td class="answer"><?php echo $data['Form1095AChange']; ?></td>
	</tr>
	<tr>
		<td>Received Form 1905-B</td>
		<td class="answer"><?php echo $data['Form1095BChange']; ?></td>
	</tr>
	<tr>
		<td>Received Form 1905-C</td>
		<td class="answer"><?php echo $data['Form1095CChange']; ?></td>
	</tr>
	<tr>
		<td>Did the client fall into an exception category?</td>
		<td class="answer"><?php echo $data['HealthExcemptChange']; ?></td>
	</tr>
	<tr>
		<td>Did the client have any foreign income or pay any foreign taxes?</td>
		<td class="answer"><?php echo $data['ForeignIncomeChange']; ?></td>
	</tr>
	<tr>
		<td>Did the client start a business or farm, purchase rental or royalty property, or acquire an interest in a partnership, S corporation, trust or REMIC?</td>
		<td class="answer"><?php echo $data['BuySellChange']; ?></td>
	</tr>
</table>
	</div></div>
<h1>Personal Information</h1>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th scope="row">Taxpayer Info</th>
    <td class="answer"><?php echo $data['FirstName']; ?></td>
    <td class="answer"><?php echo $data['MiddleName']; ?></td>
    <td class="answer"><?php echo $data['LastName']; ?></td>
    <td>Birthday: <span class="answer"><?php echo $data['Birthdate']; ?></span></td>
    <td>SSN: <span class="answer"><?php echo $data['SSN']; ?></span></td>
    <td>College: <span class="answer"><?php echo $data['TaxPayerTuition'] ?></span></td>
  </tr>
  <tr>
    <th scope="row">Spouse</th>
    <td class="answer"><?php echo $data['SpouseFirstName']; ?></td>
    <td class="answer"><?php echo $data['SpouseMiddleName']; ?></td>
    <td class="answer"><?php echo $data['SpouseLastName']; ?></td>
    <td>Birthday: <span class="answer"><?php echo $data['SpouseBirthday']; ?></span></td>
    <td>SSN: <span class="answer"><?php echo $data['SpouseSSN']; ?></span></td>
    <td>College: <span class="answer"><?php echo $data['SpouseTuition'] ?></span></td>
     </tr>
</table>
<h2>Taxpayer Contact Information</h2>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Home Phone</td>
    <td class="answer"><?php echo $data['HomePhone']; ?></td>
    <td>Mobile Phone</td>
    <td class="answer"><?php echo $data['MobilePhone']; ?></td>
    </tr>
  <tr>
    <td>Home Address</td>
    <td class="answer"><?php echo $data['Address']; ?><br />
      <?php echo $data['City']; ?>, <?php echo $data['state']; ?> <?php echo $data['Zip']; ?></td>
    <td>Email Address</td>
    <td class="answer"><?php echo $data['Email2']; ?></td>
  <tr>
    <td>&nbsp;</td>
    <td class="answer">&nbsp;</td>
    <td>Occupation</td>
    <td class="answer"><?php echo $data['Occupation']; ?></td>
    </tr>
</table>
<h1>Dependents</h1>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th scope="col">Name</th>
    <th scope="col">SSN</th>
    <th scope="col">Birthday</th>
    <th scope="col">Disabled</th>
    <th scope="col">College / Trade School</th>
    <th scope="col">Year</th>
    <th scope="col">1098-T</th>
    <th scope="col">Live at Home</th>
    </tr>
<?php foreach($data['DependentName'] as $k => $v) {
	echo "<tr>";
	echo '<td class="answer nowrap">'.$v."</td>";
	echo '<td class="answer nowrap">'.$data['DependentSSN'][$k]."</td>";
	echo '<td class="answer nowrap">'.$data['DependentBirthday'][$k]."</td>";
	echo '<td class="answer">'.$data['DependentDisabled'. ($k+1)]."</td>";
	echo '<td class="answer">'.$data['DependentCollege'. ($k+1)]."</td>";
	echo '<td class="answer">'.$data['DependentYear'. ($k+1)]."</td>";
	echo '<td class="answer">'.$data['DependentTuition'. ($k+1)]."</td>";
	echo '<td class="answer">'.$data['DependentHalftime'. ($k+1)]."</td>";
 	echo "</tr>";
//echo "<pre>"; print_r($data); echo "</pre>";
}
?>
</table>
<div class="twocol">
<div class="col">
<h1>Income</h1>
<p>The client has indicated that they have income from the following sources and has been emailed requesting supporting documents:</p>
<ul class="answer lli">
<?php if(count($enduserforms)) 
		foreach($enduserforms as $v) {
			echo "<li>" . $v . "</li>\n";
		}
?>
</ul></div>
<div class="col">
<h1>Other Items</h1>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>Did the client have any cash or investments in foreign banks or brokerage firms?</td><td class="answer"><?php echo $data['ForeignInc']; ?></td></tr>
<tr><td>Did the client purchase a home?</td><td class="answer"><?php echo $data['HomePurchase']; ?></td></tr>
<tr><td>Did the client have any non-business bad debt?</td><td class="answer"><?php echo $data['BadDebt']; ?></td></tr>
<tr><td>Did the client have any casualty loss?</td><td class="answer"><?php echo $data['Casualty']; ?></td></tr>
<tr><td>Did the client have have signature authority over a foreign bank or investment account?</td><td class="answer"><?php echo $data['ForeignSignatory']; ?></td></tr>
<tr><td>Did the client own any crypto currency?</td><td class="answer"><?php echo $data['CryptoOwnership']; ?></td></tr>
</tr>
</table>
</div>
</div><!--twocol-->

<div   style="page-break-before:always"  class="twocol">
<div class="col"><h1>Estimated Tax Payments</h1>
  <p>Did the client make estimated tax payments this year? <span class="answer"><?php echo $data['EstimatedPayments']; ?></span></p>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <th scope="col">Date Paid</th>
      <th scope="col">Amount</th>
    </tr>
    <tr>
      <td class="answer"><?php echo $data['PaymentDate1']; ?></td>
      <td class="answer"><?php echo $data['PaymentAmount1']; ?></td>
    </tr>
    <tr>
      <td class="answer"><?php echo $data['PaymentDate2']; ?></td>
      <td class="answer"><?php echo $data['PaymentAmount2']; ?></td>
    </tr>
    <tr>
      <td class="answer"><?php echo $data['PaymentDate3']; ?></td>
      <td class="answer"><?php echo $data['PaymentAmount3']; ?></td>
    </tr>
    <tr>
      <td class="answer"><?php echo $data['PaymentDate4']; ?></td>
      <td class="answer"><?php echo $data['PaymentAmount4']; ?></td>
    </tr>
  </table>
</div>
<div class="col"><h1>IRA Contributions</h1>
  <p>Did/will the client make any of the following IRA Contributions? <span class="answer"><?php echo $data['IRAContributions']; ?></span></p>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <th scope="col">&nbsp;</th>
      <th scope="col">Amount</th>
      </tr>
    <tr>
      <th scope="row">Traditional IRA - Taxpayer</th>
      <td class="answer"><?php echo $data['TaxpayerTraditionalIRA']; ?></td>
      </tr>
    <tr>
      <th scope="row">Traditional IRA - Spouse</th>
      <td class="answer"><?php echo $data['SpouseTraditionalIRA']; ?></td>
      </tr>
    <tr>
      <th scope="row">Roth IRA - Taxpayer</th>
      <td class="answer"><?php echo $data['TaxpayerRothIRA']; ?></td>
      </tr>
    <tr>
      <th scope="row">Roth IRA - Spouse</th>
      <td class="answer"><?php echo $data['SpouseRothIRA']; ?></td>
      </tr>
  </table>
</div>
</div>

<h1>Alimony and Child Care</h1>
<p>Have you made alimony payments? <span class="answer"><?php echo $data['AlimonyPayments']; ?></span></p>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th scope="col">To</th>
    <th scope="col">SSN</th>
    <th scope="col">Amount</th>
  </tr>
  <tr class="answer">
    <td><?php echo $data['AlimonyToFrom']; ?></td>
    <td><?php echo $data['AlimonySSN']; ?></td>
    <td><?php echo $data['AlimonyAmount']; ?></td>
  </tr>
</table>
<p>Have you received alimony payments? <span class="answer"><?php echo $data['AlimonyReceipt']; ?></span></p>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th scope="col">From</th>
    <th scope="col">SSN</th>
    <th scope="col">Amount</th>
  </tr>
  <tr class="answer">
    <td><?php echo $data['AlimonyFromTo']; ?></td>
    <td><?php echo $data['AlimonyFromSSN']; ?></td>
    <td><?php echo $data['AlimonyFromAmount']; ?></td>
  </tr>
</table><p>Have you made payments for childcare? <span class="answer"><?php echo $data['ChildcarePayments']; ?></span></p>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th scope="col">Provider Name</th>
    <th scope="col">Address</th>
    <th scope="col">EIN / SSN</th>
    <th scope="col">Amount</th>
  </tr>
  <tr class="answer">
    <td><?php echo $data['ChildcareProvider']; ?></td>
    <td><?php echo $data['ChildcareAddress']; ?></td>
    <td><?php echo $data['ChildcareSSN']; ?></td>
    <td><?php echo $data['ChildcareAmount']; ?></td>
  </tr>
</table>
<h1>Itemized Deductions</h1>
<div class="twocol">
<div class="col"><h2>Medical</h2>
  <ul>
    <li>Does your employer offer a cafeteria plan? <span class="answer"><?php echo $data['CafeteriaOffered']; ?></span> </li>
    <li>Do you participate in the cafeteria plan? <span class="answer"><?php echo $data['CafeteriaParticipate']; ?></span></li>
    <li>Do you have an HSA/Account? <span class="answer"><?php echo $data['HSA']; ?></span></li>
    </ul>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>Accident</td>
      <td class="answer"><?php echo $data['Accident']; ?></td>
    </tr>
    <tr>
      <td>Cancer</td>
      <td class="answer"><?php echo $data['Cancer']; ?></td>
      </tr>
    <tr>
      <td>CHIP</td>
      <td class="answer"><?php echo $data['CHIP']; ?></td>
      </tr>
    <tr>
      <td>Dental</td>
      <td class="answer"><?php echo $data['Dental']; ?></td>
      </tr>
    <tr>
      <td>Health</td>
      <td class="answer"<?php echo $data['Health']; ?></td>
      </tr>
    <tr>
      <td>Long Term Care</td>
      <td class="answer"><?php echo $data['LognTermCare']; ?></td>
      </tr>
    <tr>
      <td>Medicare</td>
      <td class="answer"><?php echo $data['Medicare']; ?></td>
      </tr>
    <tr>
      <td>Medicare Supplemental</td>
      <td class="answer"><?php echo $data['MedicareSupplemental']; ?></td>
      </tr>
    <tr>
      <td>Contact Lenses</td>
      <td class="answer"><?php echo $data['Contacts']; ?></td>
      </tr>
    <tr>
      <td>Amount paid pretax (Cafeteria Plan)</td>
      <td class="answer"><?php echo $data['PretaxAmount']; ?></td>
      </tr>
    <tr>
      <td>Prescription medicines and drugs</td>
      <td class="answer"><?php echo $data['Drugs']; ?></td>
      </tr>
    <tr>
      <td>Doctors, dentists, and nurses</td>
      <td class="answer"><?php echo $data['Doctors']; ?></td>
      </tr>
    <tr>
      <td>Hospitals and nursing homes</td>
      <td class="answer"><?php echo $data['Hospitals']; ?></td>
      </tr>
    <tr>
      <td>Glasses and contact lenses</td>
      <td class="answer"><?php echo $data['Glasses']; ?></td>
      </tr>
    <tr>
      <td>Hearing aids</td>
      <td class="answer"><?php echo $data['Hearing']; ?></td>
      </tr>
    <tr>
      <td>Travel for medical purposes (mileage)</td>
      <td class="answer"><?php echo $data['MedicalTravel']; ?></td>
      </tr>
  </table>
  </div>
<div class="col"><h2>Interest</h2>
  <p>
      <ul>
        <li>Mortgages on  primary residence total more than $1M? <span class="answer"><?php echo $data['MortgageOver1M']; ?></span>        </li>
        <li>More than $100,000 in home equity loans? <span class="answer"><?php echo $data['HomeEquity100k']; ?></span></li>
      </ul>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2">Names of Banks where client has Mortgages for Primary and Secondary Residences</td>
          <td width="15%" class="answer"><?php echo $data['BankMortgage']; ?></td>
        </tr>
        <tr>
          <td colspan="2">Residence Mortgage (Other if no form 1098 received)</td>
          <td class="answer"><?php echo $data['OtherMortgage']; ?></td>
        </tr>
        <tr>
          <td width="14%">&nbsp;</td>
          <td width="71%">Name</td>
          <td class="answer"><?php echo $data['OtherName']; ?></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>EIN</td>
          <td class="answer"><?php echo $data['OtherEIN']; ?></td>
        </tr>
        <tr>
          <td colspan="2">Points, Origination Fees</td>
          <td class="answer"><?php echo $data['Points']; ?></td>
        </tr>
      </table>
      <h2>Charitable Contributions</h2>
      <p>Written documentation for charitable contributions? <span class="answer"><?php echo $data['Documentation']; ?></span></p>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <th scope="col">&nbsp;</th>
          <th scope="col">Organization</th>
          <th scope="col">Amount</th>
        </tr>
<?php foreach($data['Organizationcash'] as $k => $v) {
	echo "<tr>";	
	echo '          <th scope="row">Cash Contributions</th>';
	echo '          <td class="answer">'.$v.'</td>';
	echo '          <td class="answer">'.$data['OrganizationAmountcash'][$k].'</td>';
	echo "</tr>";	
}  
?>
<?php foreach($data['Organizationnnoncash'] as $k => $v) {
	echo "<tr>";	
	echo '          <th scope="row">Non-Cash Contributions</th>';
	echo '          <td class="answer">'.$v.'</td>';
	echo '          <td class="answer">'.$data['OrganizationAmountnoncash'][$k].'</td>';
	echo "</tr>";	
}  
?>
<?php foreach($data['Organizationoutofpocket'] as $k => $v) {
	echo "<tr>";	
	echo '          <th scope="row">Out of Pocket Expenses</th>';
	echo '          <td class="answer">'.$v.'</td>';
	echo '          <td class="answer">'.$data['OrganizationAmountoutofpocket'][$k].'</td>';
	echo "</tr>";	
}  
?> 
        <tr>
          <th colspan="2" scope="row">Travel for Charitable Organizations<strong> (mileage)</strong></th>
          <td class="answer"><?php echo $data['Travel']; ?></td>
        </tr>
      </table>

</div></div>

<h1 style="page-break-before:always" >Itemized Deductions (continued)</h1>
<div class="twocol">
<div class="col"><h2>Taxes</h2>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <th scope="col">Real Estate Tax</th>
      <td class="answer">&nbsp;</td>
      </tr>
    <tr>
      <td>Principal Residence</td>
      <td class="answer"><?php echo $data['PrincipalResidence']; ?></td>
      </tr>
    <tr>
      <td>Second Residence</td>
      <td class="answer"><?php echo $data['SecondResidence']; ?></td>
      </tr>
    <tr>
      <td>Investment Property</td>
      <td class="answer"><?php echo $data['InvestmentProperty']; ?></td>
      </tr>
    <tr>
      <td>Other</td>
      <td class="answer"><?php echo $data['Other']; ?></td>
      </tr>
    <tr>
      <th scope="col">Personal Property Tax &amp; Sales Tax</th>
      <th scope="col">&nbsp;</th>
    </tr>
    <tr>
      <td>Boats, Trailers, Etc</td>
      <td class="answer"><?php echo $data['Boats']; ?></td>
      </tr>
    <tr>
      <td>Automobiles (not in Utah)</td>
      <td class="answer"><?php echo $data['Autos']; ?></td>
      </tr>
    <tr>
      <td>Sales Tax on Large Purchases</td>
      <td class="answer"><?php echo $data['SalesTax']; ?></td>
      </tr>
    <tr>
      <td>Sales Tax on New Vehicle</td>
      <td class="answer"><?php echo $data['VehicleSalesTax']; ?></td>
      </tr>
  </table>
</div>
<div class="col"> <h2>Miscellaneous  Deductions</h2>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>Supplies (Teachers)</td>
      <td class="answer"><?php echo $data['Supplies']; ?></td>
      </tr>
    <tr>
      <td>Equipment for Employment </td>
      <td class="answer"><?php echo $data['Equipment']; ?></td>
      </tr>
    <tr>
      <td>Gambling losses to the extent of Gambling Winnings </td>
      <td class="answer"><?php echo $data['Gambling']; ?></td>
      </tr>
    <tr>
      <td>Job Hunting Costs </td>
      <td class="answer"><?php echo $data['Job']; ?></td>
      </tr>
    <tr>
      <td>Other expense related to job not reimbursed </td>
      <td class="answer"><?php echo $data['NonReimbursed']; ?></td>
      </tr>
    <tr>
      <td>Professional Education </td>
      <td class="answer"><?php echo $data['Education']; ?></td>
      </tr>
    <tr>
      <td>Second Telephone Required by Employment </td>
      <td class="answer"><?php echo $data['Telephone']; ?></td>
      </tr>
    <tr>
      <td>Uniform Laundry </td>
      <td class="answer"><?php echo $data['Laundry']; ?></td>
      </tr>
    <tr>
      <td>Uniforms </td>
      <td class="answer"><?php echo $data['Uniforms']; ?></td>
      </tr>
    <tr>
      <td>Union and Professional Dues </td>
      <td class="answer"><?php echo $data['Dues']; ?></td>
      </tr>
  </table>
</div>
</div>

<h1>Choices</h1>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Would you like to apply refunds to next year's estimated tax payments?</td>
    <td class="answer"><?php echo $data['ApplyRefunds']; ?></td>
  </tr>
  <tr>
    <td>Would you like any refunds directly deposited?     <?php if($data['DirectDeposit'] == 'yes') {
	echo '<table style="margin:0; padding:0; width:40%; float:right;" border="0" cellspacing="0" cellpadding="0"><tr><td>Routing</td><td>'.$data['DirectDepositRouting'].'</td>';
	echo '<td>Transit</td><td>'.$data['DirectDepositAccount'].'</td></tr></table>';

 }
 ?>

    </td>
    <td class="answer"><?php echo $data['DirectDeposit']; ?></td>
  </tr>
  <tr>
    <td>Do you want to electronically file your tax return?</td>
    <td class="answer"><?php echo $data['FileElectronically']; ?></td>
    </tr>
  <tr>
    <td>If you owe tax would you like to electronically pay the tax due?  <?php if($data['ElectronicPayment'] == 'yes') {
	echo '<table style="margin:0; padding:0; width:40%; float:right;" border="0" cellspacing="0" cellpadding="0"><tr><td>Routing</td><td>'.$data['ElectronicPaymentRouting'].'</td>';
	echo '<td>Transit</td><td>'.$data['ElectronicPaymentAccount'].'</td></tr></table>';

 }
 ?>
</td>
    <td class="answer"><?php echo $data['ElectronicPayment']; ?></td>
    </tr>
  <tr>
    <td>Do you agree that Davies + Allen may speak with taxing authorities concerning this return? </td>
    <td class="answer"><?php echo $data['PreparerAuthority']; ?></td>
    </tr>
  <tr>
    <td>Would you like to donate $3 of <em><strong>primary taxpayer's</strong></em> taxes to go to the Presidential Election Campaign fund?</td>
    <td class="answer"><?php echo $data['PresidentialCampaign']; ?></td>
    </tr>
  <tr>
    <td>Would you like to donate $3 of <em><strong>spouse taxpayer's</strong></em> taxes to go to the 
      Presidential Election Campaign fund?</td>
    <td class="answer"><?php echo $data['PresidentialCampaignSpouse']; ?></td>
    </tr>
  <tr>
    <td>Would you prefer a bound paper copy of your return rather than an electronic copy?</td>
    <td class="answer"><?php echo $data['ElectronicCopy']; ?></td>
    </tr>
  <tr>
    <td>Would you like to add Audit Insurance* for $75?</td>
    <td class="answer"><?php echo $data['AuditInsurance']; ?></td>
    </tr>
</table>
<h1>Additional Notes</h1>
<p class="answer"><?php echo $data['Notes']; ?></p>
</div>

</body>
</html>