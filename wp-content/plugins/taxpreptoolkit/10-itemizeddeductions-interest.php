    <h1>Itemized Deductions - Interest</h1>
<?php include "include-complete.php"; ?>      
      <div class="formcontain">
      <h2>Mortgage Interest</h2>
      
     <div class="yesnosection">
          <p><label for="MortgageOver1M" class="label400">Do you mortgages on your primary residence total more than $1M if originated prior to 2018 or $750,000 if originated in 2018 or later?</label>
             <span class="answer"><label class="rlabel"><input type="radio" name="MortgageOver1M" id="MortgageOver1M"  class="p10f" value="yes" /> yes &nbsp;&nbsp;</label>
             <label class="rlabel"><input type="radio" name="MortgageOver1M" id="MortgageOver1M" class="p10f" value="no" /> no </label></span></p>
          <p class="notlast">
            <label for="HomeEquity100k" class="label400">Do you have more than $100,000 in home equity loans?</label>
            <span class="answer"><label class="rlabel"><input type="radio" name="HomeEquity100k" id="HomeEquity100k" class="p10f" value="yes" /> yes &nbsp;&nbsp;</label>
           <label class="rlabel"> <input type="radio" name="HomeEquity100k" id="HomeEquity100k" class="p10f" value="no" /> no </label></span></p>
   		   </div><!--/yesnosection-->
           
           
   
   <p>
     <label for="BankMortgage" >Names of Banks where you have Mortgages for Primary and Secondary Residences</label>
     <input type="text" name="BankMortgage" id="BankMortgage" class="p10f"  />
   </p>
      <p class="noline">
        <label for="OtherMortgage" >Residence Mortgage (Other if no form 1098 received)</label>
        <input name="OtherMortgage" type="text" id="OtherMortgage" class="p10f"  />
      </p>
      <p class="indent noline">
        <label for="OtherName" >Name</label>
        <input type="text" name="OtherName" id="OtherName" class="p10f"  />
      </p>
      <p class="indent">
        <label for="OtherEIN" >EIN</label>
        <input type="text" name="OtherEIN" id="OtherEIN" class="p10f"  />
      </p>
      <p>
        <label for="Points" >Points, Origination Fees</label>
        <input type="text" name="Points" id="Points" class="p10f"  />
      </p>
      
</div>

<p class="btnposition">
      <input type="button" name="submit" id="page10" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>