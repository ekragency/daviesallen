    <h1>Choices</h1>
<?php include "include-complete-asis.php"; ?>
      <div class="formcontain">
      <div class="yesnosection">
      <p>
        <label for="ApplyRefunds" class="label400">Would you like to apply refunds to next year's estimated tax payments?</label>
        <span class="answer"><label class="rlabel"><input type="radio" name="ApplyRefunds" id="ApplyRefunds" class="p13f"  value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="ApplyRefunds" id="ApplyRefunds" class="p13f" value="no" /> no</label></span></p>
        <p>
        <label for="DirectDeposit" class="label400">Would you like any refunds directly deposited?</label>
        <span class="answer"><label class="rlabel"><input type="radio" name="DirectDeposit" id="DirectDeposit" class="p13f" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="DirectDeposit" id="DirectDeposit" class="p13f" value="no" /> no</label></span></p>
	  <div id="DirectDepositForm" style="display:none;">
      <p>
        <label for="DirectDepositRouting">Bank Routing #</label>
        <input type="tel" name="DirectDepositRouting" class="p13f"  id="DirectDepositRouting" />
      </p>
      <p>
        <label for="DirectDepositAccount">Bank Account #</label>
        <input name="DirectDepositAccount" type="tel" class="p13f"  id="DirectDepositAccount" />
      </p>
</div>
    
      <p>
        <label for="FileElectronically" class="label400">Do you want to electronically file your tax return?</label>
        <span class="answer"><label class="rlabel"><input type="radio" name="FileElectronically" id="FileElectronically" class="p13f" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="FileElectronically" id="FileElectronically" class="p13f" value="no" /> no</label></span></p>
      
      <p><label for="ElectronicPayment" class="label400">If you owe tax would you like to electronically pay the tax due?</label>
        <span class="answer"><label class="rlabel"><input type="radio" name="ElectronicPayment" id="ElectronicPayment" class="p13f" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="ElectronicPayment" id="ElectronicPayment" class="p13f" value="no" /> no</label></span></p>
	  <div id="ElectronicPaymentForm" style="display:none;">
      <p>
        <label for="ElectronicPaymentRouting">Bank Routing #</label>
        <input type="tel" name="ElectronicPaymentRouting" class="p13f"  id="ElectronicPaymentRouting" />
      </p>
      <p>
        <label for="ElectronicPaymentAccount">Bank Account #</label>
        <input name="ElectronicPaymentAccount" type="tel" class="p13f"  id="ElectronicPaymentAccount" />
      </p>
</div>
      <p>
        <label for="PreparerAuthority" class="label400">Do you agree that Davies + Allen may speak with taxing authorities concerning this return?</label>
        <span class="answer"><label class="rlabel"><input type="radio" name="PreparerAuthority" id="PreparerAuthority" class="p13f" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="PreparerAuthority" id="PreparerAuthority" class="p13f" value="no" /> no</label></span></p>
      
      <p>
        <label for="PresidentialCampaign" class="label400">Would you like to donate  $3 of <em><strong>primary taxpayer's</strong></em> taxes to go to the <br />
        Presidential Election Campaign fund?</label>
        <span class="answer"><label class="rlabel"><input type="radio" name="PresidentialCampaign" id="PresidentialCampaign" class="p13f" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="PresidentialCampaign" id="PresidentialCampaign" class="p13f" value="no" /> no</label></span></p>
              <p>
                <label for="PresidentialCampaignSpouse" class="label400">Would you like to donate  $3 of <em><strong>spouse taxpayer's</strong></em> taxes to go to the <br />
                  Presidential Election Campaign fund?</label>
                <span class="answer">
                <label class="rlabel"><input type="radio" name="PresidentialCampaignSpouse" id="PresidentialCampaignSpouse" class="p13f" value="yes" />
yes </label>&nbsp;&nbsp;
<label class="rlabel"><input type="radio" name="PresidentialCampaignSpouse" id="PresidentialCampaignSpouse" class="p13f" value="no" />
no</label></span></p>

      <p>
        <label for="ElectronicCopy" class="label400">Would you prefer a bound paper copy of your return <strong>in addition</strong> to an electronic copy?</label>
        <span class="answer"><label class="rlabel"><input type="radio" name="ElectronicCopy" id="ElectronicCopy" class="p13f" value="yes" /> yes</label> &nbsp;&nbsp;<label class="rlabel"><input type="radio" name="ElectronicCopy" id="ElectronicCopy" class="p13f" value="no" /> no</label></span></p>
      
<!--
            <p class="noline">
        <label for="AuditInsurance" class="label400">Would you like to add Audit Insurance* for $75? </label>
        <span class="answer"><input type="radio" name="AuditInsurance" id="AuditInsurance" class="p13f" value="yes" /> yes &nbsp;&nbsp;<input type="radio" name="AuditInsurance" id="AuditInsurance" class="p13f" value="no" /> no</span></p>
            <p class="fineprint">*Normally  services are contracted with Davies + Allen at the time of an audit. We are creating an Audit Insurance pool to help cover these costs  in the event of an audit. If you opt-in this amount will be billed with the Tax Preparation invoice.</p>
-->

      </div><!--yesnosection-->
      </div>
    
   <p class="btnposition">
      <input type="button" name="submit" id="page13" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
