    <h1>Alimony and Child Care (for divorces finalized before Dec 31, 2018)</h1>
<?php include "include-complete.php"; ?>
              
      <div class="formcontain">
<!--ALIMONY MADE--->

      <p class="yesnosection">
        <label for="AlimonyPayments" class="label400">Have you made alimony payments?</label>
         <span class="answer"><label class="rlabel"><input type="radio" name="AlimonyPayments" class="p7f" id="AlimonyPayments" value="yes" /> yes &nbsp;&nbsp;</label>
         <label class="rlabel"><input type="radio" name="AlimonyPayments" class="p7f"  id="AlimonyPayments" value="no" /> no </label></span></p>
	  <div id="AlimonyPaymentsForm" style="display:none;">
      <p>
        <label for="AlimonyToFrom">To</label>
        <input type="text" name="AlimonyToFrom" class="p7f"  id="AlimonyToFrom" />
      </p>
      <p>
        <label for="AlimonySSN">SSN</label>
        <input name="AlimonySSN" type="text" class="p7f"  id="AlimonySSN" style="color:#aaa;" def="### - ## - ####" value="### - ## - ####"/>
      </p>
      <p>
        <label for="AlimonyAmount">Amount</label>
        <input type="text" name="AlimonyAmount"  class="p7f dollar" id="AlimonyAmount"  />
        <br />
        <br />
        <br />
        <br />
      </p>
</div>


<!-- ALIMONEY RECEIVED -->

      <p class="yesnosection"><label for="AlimonyReceipt" class="label400">Have you received alimony payments?</label>
         <span class="answer"><label class="rlabel"><input type="radio" name="AlimonyReceipt" class="p7f" id="AlimonyReceipt" value="yes" /> yes &nbsp;&nbsp;</label>
         <label class="rlabel"><input type="radio" name="AlimonyReceipt" class="p7f"  id="AlimonyReceipt" value="no" /> no </label></span></p>
	  <div id="AlimonyReceiptForm" style="display:none;">
      <p>
        <label for="AlimonyFromTo">From</label>
        <input type="text" name="AlimonyFromTo" class="p7f"  id="AlimonyFromTo" />
      </p>
      <p>
        <label for="AlimonyFromSSN">SSN</label>
        <input name="AlimonyFromSSN" type="text" class="p7f"  id="AlimonyFromSSN" style="color:#aaa;" def="### - ## - ####" value="### - ## - ####"/>
      </p>
      <p>
        <label for="AlimonyFromAmount">Amount</label>
        <input type="text" name="AlimonyFromAmount" class="p7f dollar" id="AlimonyFromAmount"  />
        <br />
        <br />
        <br />
        <br />
      </p>
</div>



     <p class="yesnosection"><label for="ChildcarePayments" class="label400">Have you made payments for childcare?</label>
         <span class="answer"><label class="rlabel"><input type="radio" name="ChildcarePayments" class="p7f" id="ChildcarePayments" value="yes" /> yes &nbsp;&nbsp;</label>
         <label class="rlabel"><input type="radio" name="ChildcarePayments" class="p7f" id="ChildcarePayments" value="no" /> no</label> </span></p>
	<div id="ChildcarePaymentsForm" style="display:none;">
     <p>
       <label for="ChildcareProvider">Provider Name</label>
       <input type="text" name="ChildcareProvider" class="p7f" id="ChildcareProvider"  />
     </p>
     <p>
       <label for="ChildcareAddress">Address</label>
       <input name="ChildcareAddress" type="text" class="p7f" id="ChildcareAddress"  />
     </p>
     <p>
       <label for="ChildcareSSN">EIN / SSN</label>
       <input type="text" name="ChildcareSSN" class="p7f" id="ChildcareSSN" style="color:#aaa;" def="### - ## - ####" value="### - ## - ####"/>
     </p>
     <p>
       <label for="ChildcareAmount">Amount Paid</label>
       <input type="text" name="ChildcareAmount" class="p7f dollar" id="ChildcareAmount"  />
     </p>
      </div>
 </div>
   <p class="btnposition">
      <input type="button" name="submit" id="page07" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
