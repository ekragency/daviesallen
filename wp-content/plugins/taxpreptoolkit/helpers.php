<?php
if(file_exists('../../../wp-load.php')) require_once('../../../wp-load.php');
else if(file_exists('wp-load.php')) require_once('wp-load.php');

if(!function_exists('taxprep_taxyear')) {
  function taxprep_taxyear() {
    $settings = (array) get_option('taxpreptoolkit_options');
    $taxyear = $settings['taxyear'];
    if(!$taxyear) $taxyear = date('Y')-1;
    return $taxyear;
  }
}

if(!function_exists('taxprep_tmppath')) {
  function taxprep_tmppath() {
    $settings = (array) get_option('taxpreptoolkit_options');
    $tmpdir = $settings['tempdir'];
    if(!$tmpdir) $tmpdir = get_home_path() . 'taxform';
    return $tmpdir;

  }
}
if(!function_exists('taxprep_gpgpath')) {
  function taxprep_gpgpath() {
    $settings = (array) get_option('taxpreptoolkit_options');
    $tmpdir = $settings['gpgpath'];
    if(!$gpgpath) $gpgpath = '/usr/bin/gpg';
    return $gpgpath;

  }
}
if(!function_exists('taxprep_getsetting')) {
  function taxprep_getsetting($setting) {
    $settings = (array) get_option('taxpreptoolkit_options');
    $set = $settings[$setting];
    return $set; 
  }
}

if(!function_exists('taxprep_get_home_path')) {
  function taxprep_get_home_path() {
    return ABSPATH;
  }
}

if(!function_exists('taxprep_get_home_url')) {
  function taxprep_get_home_url() {
    return get_home_url();
  }
}