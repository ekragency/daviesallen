    <h1>Income and Other Items</h1>
<?php include "include-complete-asis.php"; ?>
         
 <p>Do you have income from any of the following sources?</p>
 <p class="note">You will receive the list of items you check in an email after you submit this form, please gather supporting forms and documents to submit to use separately. </p>
     
      <div class="formcontain">
      <div class="leadingcheck">
      <p><input type="checkbox" name="Wages" id="Wages" value="Yes" class="p6f"/> <label for="Wages" > Wages (W-2s)</label></p>
      <p><input type="checkbox" name="StateRefund" id="StateRefund" value="Yes" class="p6f" /> <label for="StateRefund"> State tax refunds (1099-G)</label></p>
      <p><input type="checkbox" name="Interest" id="Interest" value="Yes" class="p6f" /> <label for="Interest"> Interest Income (1099-INT)</label></p>
      <p><input type="checkbox" name="Land" id="Land" value="Yes" class="p6f" /> <label for="Land" > Sales of Real Estat (1099-S)</label></p>
      <p><input type="checkbox" name="Dividend" id="Dividend" value="Yes" class="p6f" /> <label for="Dividend"> Dividend Income (1099-DIV)</label></p>
      <p><input type="checkbox" name="Partnership" id="Partnership" value="Yes" class="p6f" /> <label for="Partnership"> Partnership/S-Corporation/Trust/Estate (K1)</label></p>
      <p><input type="checkbox" name="Stock" id="Stock" value="Yes" class="p6f" /> <label for="Stock" class="Stock Sales (1099-B)"> Stock Sales (1099-B)</label></p>
      <p><input type="checkbox" name="Rental" id="Rental" value="Yes" class="p6f" /> <label for="Rental"> Rental Income (1099-MISC)</label></p>
      <p><input type="checkbox" name="IRA" id="IRA" value="Yes" class="p6f" /> <label for="IRA"> IRA Distributions/Retirement Income (1099-R)</label></p>
      <p><input type="checkbox" name="Royalty" id="Royalty" value="Yes" class="p6f" /> <label for="Royalty" > Royalty Income (1099-MISC)</label></p>
      <p><input type="checkbox" name="Commissions" id="Commissions" value="Yes" class="p6f" /> <label for="Commissions" > Commissions (1099-MISC)</label></p>
      <p><input type="checkbox" name="Prizes" id="Prizes" value="Yes" class="p6f" /> <label for="Prizes" > Prizes and Awards (1099-MISC)</label></p>
      <p><input type="checkbox" name="Unemployment" id="Unemployment" value="Yes" class="p6f" /> <label for="Unemployment"> Unemployment Income (1099-G)</label></p>
      <p><input type="checkbox" name="Gambling" id="Gambling" value="Yes" class="p6f" /> <label for="Gambling"> Gambling Winnings or Losses (W-2G)</label></p>
      <p><input type="checkbox" name="SocialSecurity" id="SocialSecurity" value="Yes" class="p6f" /> <label for="SocialSecurity"> Social Security Income (SSA-1099)</label></p>
      <p><input type="checkbox" name="Debt" id="Debt" value="Yes" class="p6f" /> <label for="Debt"> Debt Forgiveness (1099-C)</label></p>
      <p><input type="checkbox" name="Farming" id="Farming" value="Yes" class="p6f" /> <label for="Farming"> Farming Government Programs <br class="mobile" />(1099-PATR)</label></p>
      </div><!--leadingcheck-->	 
      <div class="yesnosection">
      <p><label for="ForeignInc" class="label400">Did you have any cash or investments in foreign banks or brokerage firms?</label>
      <span class="answer"><label class="rlabel"><input type="radio" name="ForeignInc" id="ForeignInc" value="yes" class="p6f" /> yes &nbsp;&nbsp;</label>
         <label class="rlabel"><input type="radio" name="ForeignInc" id="ForeignInc" value="no" class="p6f" /> no</label></span> </p>
<p><label for="ForeignSignatory" class="label400">Do you have signature authority over a foreign bank or investment account?</label>
<span class="answer"><label class="rlabel"><input type="radio" name="ForeignSignatory" id="ForeignSignatory" value="yes" class="p6f" /> yes &nbsp;&nbsp;</label>
   <label class="rlabel"><input type="radio" name="ForeignSignatory" id="ForeignSignatory" value="no" class="p6f" /> no</label></span> </p>
<p class="nobot"><label for="CryptoOwnership" class="label400">Do you own any crypto currency?</label>
<span class="answer"><label class="rlabel"><input type="radio" name="CryptoOwnership" id="CryptoOwnership" value="yes" class="p6f" /> yes &nbsp;&nbsp;</label>
   <label class="rlabel"><input type="radio" name="CryptoOwnership" id="CryptoOwnership" value="no" class="p6f" /> no</label></span> </p>

<h2>Income</h2>
  <p class="nobot">
   <label for="ForeignIncomeChange" class="label400">Did you have any foreign income or pay any foreign taxes?</label>
     <span class="answer"><label class="rlabel"><input type="radio" name="ForeignIncomeChange" class="p6f" id="ForeignIncomeChange" value="yes" /> yes &nbsp;&nbsp;</label><label class="rlabel"><input type="radio" name="ForeignIncomeChange" id="ForeignIncomeChange"  class="p6f" value="no" /> no </label>     </span>			  	
  </p>

  <h2>Purchases, Sales and Debt</h2>
  <p class="">
     <label for="BuySellChange" class="label400">Did you start a business or farm, purchase rental or royalty property, or acquire an interest in a partnership, S corporation, trust or REMIC?</label>
     <span class="answer"><label class="rlabel"><input type="radio" name="BuySellChange" class="p6f" id="BuySellChange" value="yes" /> yes &nbsp;&nbsp;</label><label class="rlabel"><input type="radio" name="BuySellChange" id="BuySellChange"  class="p6f" value="no" /> no </label>     </span>			  	
  </p>
<p><label for="HomePurchase" class="label400">Did you purchase a home?</label>
<span class="answer"><label class="rlabel"><input type="radio" name="HomePurchase" id="HomePurchase" value="yes" class="p6f" /> yes &nbsp;&nbsp;</label>
<label class="rlabel"><input type="radio" name="HomePurchase" id="HomePurchase" value="no" class="p6f" />  no</label></span> </p>

<p><label for="BadDebt" class="label400">Did you have any non-business bad debt?</label>
<span class="answer"><label class="rlabel"><input type="radio" name="BadDebt" id="BadDebt" value="yes" class="p6f" /> yes &nbsp;&nbsp;</label>
<label class="rlabel"><input type="radio" name="BadDebt" id="BadDebt" value="no" class="p6f" />  no</label></span> </p>

<p><label for="Casualty" class="label400">Did you have any casualty loss in a presidentially declared disaster area?</label>
<span class="answer"><label class="rlabel"><input type="radio" name="Casualty" id="Casualty" value="yes" class="p6f" /> yes &nbsp;&nbsp;</label>
<label class="rlabel"><input type="radio" name="Casualty" id="Casualty" value="no" class="p6f" />  no</label></span> </p>	  	

</div>
</div>
 
   <p class="btnposition">
      <input type="button" name="submit" id="page06" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
