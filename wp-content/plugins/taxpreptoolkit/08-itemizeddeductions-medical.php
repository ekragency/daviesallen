    <h1>Itemized Deductions - Medical</h1>
<?php include "include-complete.php"; ?>
              
      <div class="formcontain">
          <div class="yesnosection">
          <p><label for="CafeteriaOffered" class="label400">Does your employer offer a cafeteria plan?</label>
             <span class="answer"><label class="rlabel"><input type="radio" name="CafeteriaOffered" id="CafeteriaOffered"  class="p8f" value="yes" /> yes &nbsp;&nbsp;</label>
             <label class="rlabel"><input type="radio" name="CafeteriaOffered" id="CafeteriaOffered" class="p8f" value="no" /> no </label></span></p>
          <p>
            <label for="CafeteriaParticipate" class="label400">Do you participate in the cafeteria plan?</label>
            <span class="answer"><label class="rlabel"><input type="radio" name="CafeteriaParticipate" id="CafeteriaParticipate" class="p8f" value="yes" /> yes &nbsp;&nbsp;</label>
           <label class="rlabel"> <input type="radio" name="CafeteriaParticipate" id="CafeteriaParticipate" class="p8f" value="no" /> no </label></span></p>
          <p>
            <label for="HSA" class="label400">Do you have an HSA account?</label>
            <span class="answer"><label class="rlabel"><input type="radio" name="HSA" id="HSA" class="p8f" value="yes" /> yes &nbsp;&nbsp;</label>
			<label class="rlabel"><input type="radio" name="HSA" id="HSA" class="p8f" value="no" /> no </label></span></p>
          <p id="HSAForm" class="indent" style="display:none;">
            <label for="HSAOwner" class="label400">Who is the HSA Owner?</label>
            <span class="answer"><label class="rlabel"><input type="radio" name="HSAOwner" id="HSAOwner" class="p8f" value="yes" />Employer &nbsp;&nbsp;
            <label class="rlabel"><input type="radio" name="HSAOwner" id="HSAOwner" class="p8f" value="no" />Self </span></p>
   		   </div><!--/yesnosection-->
<h2>Out of Pocket Expenses</h2>
<div class="itemizeddeductions">
      <p><label for="Accident" >Accident</label>
        <input type="text" name="Accident" id="Accident" class="p8f dollar"  />
      </p>
      <p>
        <label for="Cancer" >Cancer</label>
        <input type="text" name="Cancer" id="Cancer" class="p8f dollar"  /> 
      </p>
      <p><label for="CHIP" >CHIP</label>
        <input type="text" name="CHIP" id="CHIP" class="p8f dollar"  />
      </p>
      <p>
        <label for="Dental" >Dental</label>
        <input type="text" name="Dental" id="Dental" class="p8f dollar"  /> 
      </p>
      <p><label for="Health" >Health</label>
        <input type="text" name="Health" id="Health" class="p8f dollar"  />
      </p>
      <p>
        <label for="LognTermCare" >Long Term Care</label>
        <input type="text" name="LognTermCare" id="LognTermCare" class="p8f dollar"  /> 
      </p>
      <p><label for="Medicare" >Medicare</label>
        <input type="text" name="Medicare" id="Medicare" class="p8f dollar"  />
      </p>
      <p>
        <label for="MedicareSupplemental" >Medicare Supplemental</label>
        <input type="text" name="MedicareSupplemental" id="MedicareSupplemental" class="p8f dollar"  /> 
      </p>
      <p><label for="Contacts" >Contact Lenses</label>
        <input type="text" name="Contacts" id="Contacts" class="p8f dollar"  />
      </p>
      <p>
        <label for="PretaxAmount" >Amount paid pretax (Cafeteria Plan)</label>
        <input type="text" name="PretaxAmount" id="PretaxAmount" class="p8f dollar"  /> 
      </p>
      <p><label for="Drugs" >Prescription medicines and drugs</label>
        <input type="text" name="Drugs" id="Drugs" class="p8f dollar"  />
      </p>
      <p>
        <label for="Doctors" >Doctors, dentists, and nurses</label>
        <input type="text" name="Doctors" id="Doctors" class="p8f dollar"  /> 
      </p>
      <p><label for="Hospitals" >Hospitals and nursing homes</label>
        <input type="text" name="Hospitals" id="Hospitals" class="p8f dollar"  />
      </p>
      <p>
        <label for="Glasses" >Glasses and contact lenses</label>
        <input type="text" name="Glasses" id="Glasses" class="p8f dollar"  /> 
      </p>
      <p><label for="Hearing" >Hearing aids</label>
        <input type="text" name="Hearing" id="Hearing" class="p8f dollar"  />
      </p>
      <p>
        <label for="MedicalTravel" >Travel for medical purposes </label>
        <input name="MedicalTravel" type="text" id="MedicalTravel" class="p8f" style="color:#aaa" def="(in number of miles)" value="(in number of miles)"/> 
      </p>
      </div>
</div>
   <p class="btnposition">
      <input type="button" name="submit" id="page08" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
