<?php require_once "helpers.php"; 
  $TAXYEAR = taxprep_taxyear(); 
?>
    <div id="intro">
    <hr />
    <h1> <?php echo $TAXYEAR ?> Tax Prep Toolkit</h1>
    <hr />
  </div>
  <!--/intro-->

<div id="completion">You have <strong id="countdown">72 hours</strong> left to complete<br class="mobile" /> and submit this information.<br><strong>Use the checkboxes at the top of each section to mark that section as complete.</strong></div>

<div id="taxorganizer">
 
    <div class="menucontrols">
    <!-- IF there are NO sections open display this one: -->
        <div id="openall" class="openclose"><a href="#"><span class="open"></span>Open All Sections</a></div>
    <!-- IF there are ANY sections open display this one: -->
        <div id="closeall" class="openclose"><a href="#"><span class="close"></span>Close All Sections</a></div>
        
       
    </div><!--/menucontrols-->
    

 <form name="taxform" id="taxform" action="" method="POST">

        <div class="menu"> <ul id="taxmenu">
	  <li id="p0" href="0">&#9656; Miscellaneous Questions </li>
<div id="c0" class="contentblock"><?php include "00-misc-questions.php"; ?></div>
	  
      <li id="p1" href="1">&#9656; Primary Taxpayer Info </li>
<div id="c1" class="contentblock"><?php include "01-taxpayerinfo.php"; ?></div>
      <li id="p2" href="2">&#9656; Spouse Info </li>
<div id="c2" class="contentblock"><?php include "02-spouseinformation.php"; ?> </div>
      <li id="p3" href="3">&#9656; Dependent Info </li>
<div id="c3" class="contentblock"><?php include "03-dependentinformation.php"; ?> </div>
      <li id="p4" href="4">&#9656; Estimated Tax Payments  </li>
<div id="c4" class="contentblock"><?php include "04-esimatedtaxpayments.php"; ?> </div>
      <li id="p5" href="5">&#9656; IRA Contributions </li>
<div id="c5" class="contentblock"><?php include "05-IRA-contributions.php"; ?> </div>
      <li id="p6" href="6">&#9656; Income and Other Items  </li>
<div id="c6" class="contentblock"><?php include "06-incomeotheritems.php"; ?> </div>
      <li id="p7" href="7">&#9656; Alimony and Child Care </li>
<div id="c7" class="contentblock"><?php include "07-alimonychildcare.php"; ?> </div>
      <li id="p8" href="8">&#9656; Itemized Deductions - Medical  </li>
<div id="c8" class="contentblock"><?php include "08-itemizeddeductions-medical.php"; ?> </div>
      <li id="p9" href="9">&#9656; Itemized Deductions - Taxes  </li>
<div id="c9" class="contentblock"><?php include "09-itemizeddeductions-taxes.php"; ?> </div>
      <li id="p10" href="10">&#9656; Itemized Deductions - Interest  </li>
<div id="c10" class="contentblock"><?php include "10-itemizeddeductions-interest.php"; ?> </div>
      <li id="p11" href="11">&#9656; Itemized Deductions - Misc<span class="hideonmobile">ellaneous</span>  </li>
<div id="c11" class="contentblock"><?php include "11-itemizeddeductions-misc.php"; ?> </div>
      <li id="p12" href="12">&#9656; Itemized Deductions - <br class="mobile" />Charitable Contributions  </li>
<div id="c12" class="contentblock"><?php include "12-itemizeddeductions-charitable.php"; ?> </div>
      <li id="p13" href="13">&#9656; Choices </li>
<div id="c13" class="contentblock"><?php include "13-elections.php"; ?> </div>
	<li id="p14" href="14">&#9656; Additional Info  </li>
<div id="c14" class="contentblock"><?php include "14-additionalnotes.php"; ?> </div>
<!--       <li id="p15" href="15">&#9656; Submit   to Davies + Allen</li> -->
<div class="contentblock" style="display:block !important;"><?php include "15-agreesubmit.php"; ?></div>

    </ul>
        </div>
 </form></div><!--/taxorganizer-->

