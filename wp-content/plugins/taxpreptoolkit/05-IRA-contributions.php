    <h1>IRA Contributions</h1>
<?php include "include-complete.php"; ?>
<div class="formcontain">  
    <p class="yesnosection noline">
    <label for="IRAContributions" class="label400">Have you or will you make any traditional or ROTH IRA contributions for <?php echo $TAXYEAR; ?>?</label>
    <span class="answer"><label class="rlabel"><input type="radio" name="IRAContributions" id="IRAContributions"  class="p5f" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="IRAContributions" id="IRAContributions"  class="p5f" value="no" /> no</label></span></p>
    
    <div id="IRAContributionsForm" style="display:none;">
        <h2>Traditional IRA</h2>
            <p>
            <label for="TaxpayerTraditionalIRA" >Traditional IRA (Taxpayer)</label>
            <input name="TaxpayerTraditionalIRA" type="text" id="TaxpayerTraditionalIRA" class="p5f dollar" />
            </p>
            <p class="noline">
            <label for="SpouseTraditionalIRA" >Traditional IRA (Spouse)</label>
            <input type="text" name="SpouseTraditionalIRA" id="SpouseTraditionalIRA" class="p5f dollar"   /></p>
        <h2>Roth IRA</h2>
            <p>
            <label for="TaxpayerRothIRA" >Roth IRA (Taxpayer)</label>
            <input name="TaxpayerRothIRA" type="text" id="TaxpayerRothIRA" class="p5f dollar"  />
            </p>
            <p>
            <label for="SpouseRothIRA" >Roth IRA (Spouse)</label>
            <input type="text" name="SpouseRothIRA" id="SpouseRothIRA" class="p5f dollar"  />
            </p>
    </div>
</div><!--formcontain-->

<p class="btnposition">
<input type="button" name="submit" id="page05" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
