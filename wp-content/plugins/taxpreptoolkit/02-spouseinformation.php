    <h1>Spouse Information</h1>
<?php include "include-complete.php"; ?>
    <div class="formcontain">
      <p><label for="SpouseFirstName">First Name</label>
        <input type="text" name="SpouseFirstName" id="SpouseFirstName" class="p2f"  />
      </p>
      <p><label for="SpouseMiddleName">Middle Name</label>
        <input type="text" name="SpouseMiddleName" id="SpouseMiddleName" class="p2f"  />
</p>
      <p><label for="SpouseLastName">Last Name</label>
        <input type="text" name="SpouseLastName" id="SpouseLastName" class="p2f"  />
</p>
      <p>
        <label for="SpouseOccupation">Occupation</label>
        <input type="text" name="SpouseOccupation" id="SpouseOccupation" class="p2f"  />
</p>
      <p>
        <label for="SpouseBirthday">Birthday</label>
        <input type="tel" name="SpouseBirthday" id="SpouseBirthday" class="p2f"   def="## / ## / ####" value="## / ## / ####"/>
</p>
      <p>
        <label for="SpouseSSN">SSN</label>
        <input name="SpouseSSN" type="tel" id="SpouseSSN" style="color:#AAA" class="p2f" def="### - ## - ####" value="### - ## - ####"/>
      </p>
<h2>College and Secondary Education</h2>
<p class="nobot">
   <label for="SpouseTuition" class="label400">Did your spouse pay tuition, fees, cost of books and supplies, for education at a university, college or other post secondary educational institution?</label>
     <span class="answer"><label class="rlabel"><input type="radio" name="SpouseTuition" class="p1f" id="SpouseTuition" value="yes" /> yes &nbsp;&nbsp;</label><label class="rlabel"><input type="radio" name="SpouseTuition" id="SpouseTuition"  class="p1f" value="no" /> no </label>     </span>			  	
  </p>
       
    </div>
   <p class="btnposition">
        <input type="button" name="submit" id="page02" tabindex="-1" value="Continue to Next section &rsaquo;" class="sbutton btn" />
      </p> 
    
