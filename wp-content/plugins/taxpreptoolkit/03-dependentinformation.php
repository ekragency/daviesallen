    <h1>Dependent Information</h1>
    <?php include "include-complete.php"; ?>

      <p>Please provide the following information for dependent children and others that <u>reside in your home</u> or full time students under the age of 24 that you feel might be qualified to be claimed by you as a dependent.</p>
      <p class="addsubtract"><a class="depadd" href="#"><img src="/wp-content/plugins/taxpreptoolkit/taxorg-images/icon-circle-plus.png" width="30" height="30" alt="Add" /><span> Add Dependent</span></a></p>
    <p id="p3submit" class="btnposition">
      <input type="button" name="submit" id="page03" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
