    <h1>Itemized Deductions - Taxes (maximum $10,000)</h1>
<?php include "include-complete.php"; ?>
      
              
      <div class="formcontain">
      <h2>Real Estate Tax</h2>
      <div class="itemizeddeductions">
   <p>
     <label for="PrincipalResidence"  >Principal Residence</label>
      <input type="text" name="PrincipalResidence" id="PrincipalResidence" class="p9f dollar"  />
      </p>
      <p>
        <label for="SecondResidence"  >Second Residence</label>
        <input name="SecondResidence" type="text" id="SecondResidence" class="p9f dollar"  />
      </p>
      <p>
        <label for="InvestmentProperty"  >Investment Property</label>
        <input type="text" name="InvestmentProperty" id="InvestmentProperty" class="p9f dollar"  />
      </p>
      <p class="noline">
        <label for="Other"  >Other</label>
        <input type="text" name="Other" id="Other" class="p9f dollar"  />
      </p>
     
    <h2>  Personal Property Tax & Sales Tax</h2>
      

     <p>
       <label for="Boats"  >Boats, Trailers, Etc</label>
       <input type="text" name="Boats" id="Boats" class="p9f dollar"  />
     </p>
     <p>
       <label for="Autos"  >Automobiles</label>
       <input name="Autos" type="text" id="Autos" class="p9f dollar" style="color:#aaa;" def="(Not in Utah)" value="(Not in Utah)"/>
     </p>
     <p>
       <label for="SalesTax"  >Sales Tax on Large Purchases</label>
       <input type="text" name="SalesTax" id="SalesTax" class="p9f dollar"  />
     </p>
     <p>
       <label for="VehicleSalesTax"  >Sales Tax on New Vehicle</label>
       <input type="text" name="VehicleSalesTax" id="VehicleSalesTax" class="p9f dollar"  />
     </p>
      </div></div>
    <p class="btnposition">
      <input type="button" name="submit" id="page9" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
