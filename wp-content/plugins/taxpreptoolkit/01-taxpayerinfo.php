	    <h1>Taxpayer Information</h1>
<?php include "include-complete.php"; ?>
    <div class="formcontain">
      <p><label for="FirstName">First Name</label>
        <input type="text" name="FirstName" id="FirstName" class="p1f"  />
      </p>
      <p><label>Middle Name</label>
        <input type="text" name="MiddleName" id="MiddleName" class="p1f"  />
</p>
      <p><label for="LastName">Last Name</label>
        <input type="text" name="LastName" id="LastName" class="p1f"  />
</p>
      <p>
        <label for="Birthdate">Birthday</label> 
        <input type="tel" name="Birthdate" id="Birthdate" class="p1f"   def="## / ## / ####" value="## / ## / ####" />
<p>
  <label for="Occupation">Occupation</label>
<input type="text" name="Occupation" id="Occupation" class="p1f"  />
</p>        
        
        
</p>
      <p class="">
        <label for="SSN">SSN</label>
        <input name="SSN" type="tel" id="SSN" style="color:#AAA" class="p1f" def="### - ## - ####" value="### - ## - ####"/>
</p>

      <h2>Contact Information</h2>
      <p>
        <label for="HomePhone">Home Phone</label>
        <input name="HomePhone" type="tel" class="p1f" id="HomePhone" style="color:#AAA;" def="( ### ) ### - ####" value="( ### ) ### - ####"/></p>
      <p>
        <label for="MobilePhone">      
        Mobile Phone</label>
        <input name="MobilePhone" type="tel" class="p1f" id="MobilePhone" style="color:#AAA" def="( ### ) ### - ####" value="( ### ) ### - ####"/>
      </p>
      <p class="noline">
        <label for="Email2">Email Address</label>
        <input type="text" autocorrect="off" autocapitalize="off" spellcheck="false" name="Email2" class="p1f" id="Email2"  />
      </p>
      <h2>Home Address</h2>
      <p>
        <label for="Address">
        Home Address</label>
        <input type="text" name="Address" id="Address" class="p1f"  />
</p>
      <p>
        <label for="City">Home City</label>
        <input type="text" name="City" id="City" class="p1f"  />
</p>
      <p>
        <label for="State">Home State</label>
        <select name="state">
	<option value="AL">Alabama</option>
	<option value="AK">Alaska</option>
	<option value="AZ">Arizona</option>
	<option value="AR">Arkansas</option>
	<option value="CA">California</option>
	<option value="CO">Colorado</option>
	<option value="CT">Connecticut</option>
	<option value="DE">Delaware</option>
	<option value="DC">District of Columbia</option>
	<option value="FL">Florida</option>
	<option value="GA">Georgia</option>
	<option value="HI">Hawaii</option>
	<option value="ID">Idaho</option>
	<option value="IL">Illinois</option>
	<option value="IN">Indiana</option>
	<option value="IA">Iowa</option>
	<option value="KS">Kansas</option>
	<option value="KY">Kentucky</option>
	<option value="LA">Louisiana</option>
	<option value="ME">Maine</option>
	<option value="MD">Maryland</option>
	<option value="MA">Massachusetts</option>
	<option value="MI">Michigan</option>
	<option value="MN">Minnesota</option>
	<option value="MS">Mississippi</option>
	<option value="MO">Missouri</option>
	<option value="MT">Montana</option>
	<option value="NE">Nebraska</option>
	<option value="NV">Nevada</option>
	<option value="NH">New Hampshire</option>
	<option value="NJ">New Jersey</option>
	<option value="NM">New Mexico</option>
	<option value="NY">New York</option>
	<option value="NC">North Carolina</option>
	<option value="ND">North Dakota</option>
	<option value="OH">Ohio</option>
	<option value="OK">Oklahoma</option>
	<option value="OR">Oregon</option>
	<option value="PA">Pennsylvania</option>
	<option value="RI">Rhode Island</option>
	<option value="SC">South Carolina</option>
	<option value="SD">South Dakota</option>
	<option value="TN">Tennessee</option>
	<option value="TX">Texas</option>
	<option value="UT" selected="selected">Utah</option>
	<option value="VT">Vermont</option>
	<option value="VA">Virginia</option>
	<option value="WA">Washington</option>
	<option value="WV">West Virginia</option>
	<option value="WI">Wisconsin</option>
	<option value="WY">Wyoming</option>
</select>
</p>
</p>
<p >
  <label for="Zip">Home Zip</label>
  <input type="tel" name="Zip" id="Zip" class="p1f" />
</p>    


<h2>College and Secondary Education</h2>
<p class="nobot">
   <label for="TaxPayerTuition" class="label400">Did you pay tuition, fees, cost of books and supplies, for education at a university, college or other post secondary educational institution?</label>
     <span class="answer"><label class="rlabel"><input type="radio" name="TaxPayerTuition" class="p1f" id="TaxPayerTuition" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="TaxPayerTuition" id="TaxPayerTuition"  class="p1f" value="no" /> no </label>     </span>			  	
  </p>

    </div>
<p class="btnposition"> <input type="button" name="submit" id="page01" tabindex="-1" value="Continue to Next section &rsaquo;" class="sbutton btn" /> </p>
   
    
 