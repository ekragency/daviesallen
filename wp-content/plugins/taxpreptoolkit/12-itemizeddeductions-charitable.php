    <h1>Itemized Deductions - Charitable Contributions</h1>
<?php include "include-complete.php"; ?>
      <div class="formcontain">
	  <p class="noline yesnosection"><label for="Documentation" class="label400">Do you have written documentation for your charitable contributions?</label>
        <span class="answer"><label class="rlabel"><input type="radio" name="Documentation" class="p12f" id="Documentation" value="yes" /> yes &nbsp;&nbsp;</label><label class="rlabel"><input type="radio" class="p12f"  name="Documentation" id="Documentation" value="no" /> no</label></span>      </p>

<h2>Cash Contributions</h2>
<p id="addsub1" class="addsubtract"><a class="orgadd" data="cash" href="#"><img src="/wp-content/plugins/taxpreptoolkit/taxorg-images/icon-circle-plus.png" width="30" height="30" alt="Add" /> <span>Add Organization</span></a> </p>
<h2>Non Cash Contributions</h2>
<p id="addsub3" class="addsubtract"><a class="orgadd" data="noncash" href="#"><img src="/wp-content/plugins/taxpreptoolkit/taxorg-images/icon-circle-plus.png" width="30" height="30" alt="Add" /> <span>Add Organization</span></a> </p>
<h2>Out of Pocket Expenses</h2>
<p id="addsub2"  class="addsubtract"><a class="orgadd" data="outofpocket" href="#"><img src="/wp-content/plugins/taxpreptoolkit/taxorg-images/icon-circle-plus.png" width="30" height="30" alt="Add" /> <span>Add Organization</span></a> </p>
<h2>Travel</h2><p>
  <label for="Travel" >Travel for Charitable Organizations</label>
  <input name="Travel" type="text" id="Travel" class="p12f" style="color:#aaa;" def="(Mileage)"  value="(Mileage)"/>
</p>
      </div>
    <p class="btnposition">
      <input type="button" name="submit" id="page12" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
