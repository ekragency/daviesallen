<style>
p.nobot { border-bottom:none !important; }
</style>
<h1>Miscellaneous Questions</h1>
<?php include "include-complete-asis.php"; ?>
      <div class="formcontain">
	  	<div class="yesnosection">
		  	<h2>Personal Information</h2>
		  	<p>
		  		<label for="MaritalStatusChange" class="label400">Did your marital status change this year?</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="MaritalStatusChange" class="p0f" id="MaritalStatusChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="MaritalStatusChange" id="MaritalStatusChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  	</p>
		  	<p>
		  		<label for="AddressChange" class="label400">Did your address change during the year?</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="AddressChange" class="p0f" id="AddressChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="AddressChange" id="AddressChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  	</p>
		  	<p class="nobot">
		  		<label for="DependentStatusChange" class="label400">Could you be claimed as a dependent on another person's tax return for <?php echo $TAXYEAR; ?>?</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="DependentStatusChange" class="p0f" id="DependentStatusChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="DependentStatusChange" id="DependentStatusChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  	<h2>Dependents</h2>
		  	</p>
		  	<p>
		  		<label for="DependentsChange" class="label400">Were there any changes in dependents?</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="DependentsChange" class="p0f" id="DependentsChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="DependentsChange" id="DependentsChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  	</p>
		  	<p>
		  		<label for="UnmarriedChildrenChange" class="label400">Were any of your unmarried children who might be claimed as dependents 19 years of age or older at the end of <?php echo $TAXYEAR; ?>?</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="UnmarriedChildrenChange" class="p0f" id="UnmarriedChildrenChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="UnmarriedChildrenChange" id="UnmarriedChildrenChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  	</p>
		  	<p class="nobot">
		  		<label for="DependentIncomeChange" class="label400">Did you have any children under age 18 at the end of <?php echo $TAXYEAR; ?> or full-time students under age 24 at the end of <?php echo $TAXYEAR; ?>, with more than $2100 of unearned income?</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="DependentIncomeChange" class="p0f" id="DependentIncomeChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="DependentIncomeChange" id="DependentIncomeChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  	</p>
		  	<h2>Economic Impact Payments</h2>
				<p>
						<label for="EIPA" class="label400">Did you receive an amount from the first stimulus rebate? (Economic Impact Payments)?</label>
						<span class="answer"><label class="rlabel"><input type="radio" id="EIPA" name="EIPA" class="p0f" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio"  id="EIPA" name="EIPA" class="p0f" value="no" /> no</label> </span>
				</p>
				<div id="EIPAForm" style="display:none">
					<p>
						<label for="EIPAval" >How much did you receive?</label>
						<input name="EIPAval" type="text" id="EIPAval" class="p0f dollar" />
						</p>

				</div>
	<label for="EIPB" class="label400">Did you receive an amount from the second stimulus rebate? (Economic Impact Payments)?</label>
	<span class="answer"><label class="rlabel"><input type="radio" id="EIPB" name="EIPB" class="p0f" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio"  id="EIPB" name="EIPB" class="p0f" value="no" /> no</label> </span>
</p>
<div id="EIPBForm" style="display:none">
<p>
	<label for="EIPBval" >How much did you receive?</label>
	<input name="EIPBval" type="text" id="EIPBval" class="p0f dollar" />
	</p>

</div>
	  	<h2>Health Care Coverage</h2>
		  	<p>
			  	<label for="HealthCoverageChange" class="label400">Did you and your dependents have healthcare coverage for the full year?</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="HealthCoverageChange" class="p0f" id="HealthCoverageChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="HealthCoverageChange" id="HealthCoverageChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  	</p>
	  	    <p class="nobot" style="padding-left:20px;">
		  		Did you receive any of the following IRS Documents?
	  	    </p>
	  	    <p style="">
		  	
		  		<label for="Form1095AChange" class="label400">Form 1095-A (Health Insurance Marketplace Statement)</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="Form1095AChange" class="p0f" id="Form1095AChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="Form1095AChange" id="Form1095AChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  		<label for="Form1095BChange" class="label400">Form 1095-B (Health Coverage)</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="Form1095BChange" class="p0f" id="Form1095BChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="Form1095BChange" id="Form1095BChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  		<label for="Form1095CChange" class="label400">Form 1095-C (Employer Provided Health Insurance Offer and Coverage)</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="Form1095CChange" class="p0f" id="Form1095CChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="Form1095CChange" id="Form1095CChange"  class="p0f" value="no" /> no</label>      </span>			  	

		  	</p>
		  	<p class="nobot">
		  		<label for="HealthExcemptChange" class="label400">If you or your dependents did not have health care coverage during the year, do you fall into one of the following exception categories:<br>
		  		Indian tribe membership, 
		  		Health sharing ministry membership, 
		  		Religious sect membership, 
		  		Incarceration or
		  		Excempt non-citizen or economic hardship</label>
		  		<span class="answer"><label class="rlabel"><input type="radio" name="HealthExcemptChange" class="p0f" id="HealthExcemptChange" value="yes" /> yes </label>&nbsp;&nbsp;<label class="rlabel"><input type="radio" name="HealthExcemptChange" id="HealthExcemptChange"  class="p0f" value="no" /> no</label>      </span>			  	
		  		
		  	</p>		  		

	  	</div>
      </div>
      
<p class="btnposition">
<input type="button" name="submit" id="page00" value="Continue to Next section &rsaquo;" class="sbutton btn" />
</p>
