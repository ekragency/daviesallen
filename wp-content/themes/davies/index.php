<?php get_header('default'); ?>

    <main role="main">
        <section class="posts__page">
            <div class="uk-container">
                <div uk-grid>
                    <div class="uk-width-2-3@m">
                        <div class="main-blog">
                            <h1><?php wp_title(''); ?></h1>
                            <?php get_template_part('loop'); ?>

                            <!-- pagination -->
                            <div class="pagination">
                                <?php html5wp_pagination(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-expand@m">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </section>
    </main>

<?php get_footer('default'); ?>
