<!doctype html>
<html <?php language_attributes(); ?> class="no-js">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

	<link href="//www.google-analytics.com" rel="dns-prefetch">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php bloginfo('description'); ?>">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K79GXR2');</script>
	<!-- End Google Tag Manager -->

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

	<?php wp_head(); ?>
	<script>
		// conditionizr.com
		// configure environment tests
		conditionizr.config({
			assets: '<?php echo get_template_directory_uri(); ?>',
			tests: {}
		});
	</script>

</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K79GXR2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<header class="header">
		<div class="uk-container">
			<div class="uk-flex uk-flex-middle" uk-grid>
				<div class="uk-width-auto">
					<a href="<?= bloginfo('url') ?>">
						<img src="<?= get_template_directory_uri() ?>/img/logo.svg" alt="JA Logo">
					</a>
				</div>
				<div class="uk-width-expand uk-visible@l uk-text-middle">
					<?= wp_nav_menu(array('menu'=>'Main Menu')) ?>
				</div>
				<div class="uk-width-expand uk-text-right uk-hidden@l hidden-button">
					<button class="menu-icon" uk-toggle="target: #mobile-menu"><i class="fas fa-bars"></i></button>
				</div>
			</div>
		</div>
	</header>

	<!-- This is the off-canvas -->
	<div id="mobile-menu" uk-offcanvas>
		<div class="uk-offcanvas-bar">
			<button class="uk-offcanvas-close" type="button" uk-close></button>
			<div class="mini-header">
				<a href="<?= bloginfo('url') ?>">
					<img src="<?= get_template_directory_uri() ?>/img/logo.svg" alt="JA Logo">
				</a>
			</div>
			<?= wp_nav_menu(array('menu' => 'Main Menu' )) ?>
		</div>
	</div>