<?php get_header(); ?>

<main role="main">
	<section class="posts__page">
		<div class="uk-container">

			<div class="headline uk-text-center">
				<h1><?php _e( 'Archives', 'html5blank' ); ?></h1>
			</div>
		
			<?php get_template_part('loop'); ?>

			<!-- pagination -->
			<div class="pagination">
				<?php html5wp_pagination(); ?>
			</div>
	</section>
</main>

<?php get_footer(); ?>
