<?php
	if( is_page('home') ) {
		get_header();
	} else {
		get_header('default');
	}
?>

<main class="<?php if($bgd) echo 'has-bg-design' ?>">
	<?php while(have_posts()) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>
</main>

<?php get_footer('default'); ?>