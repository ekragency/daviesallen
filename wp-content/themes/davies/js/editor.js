wp.domReady( () => {
	wp.blocks.unregisterBlockStyle( 'core/button', 'default' );
	wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );
    wp.blocks.unregisterBlockStyle( 'core/button', 'squared' );
    wp.blocks.unregisterBlockStyle( 'core/button', 'fill' );

    wp.blocks.registerBlockStyle( 'core/group', {
        name: 'large',
        label: 'Large Container'
    } );

    wp.blocks.registerBlockStyle( 'core/heading', {
        name: 'small',
        label: 'Small Container'
    } );

    wp.blocks.registerBlockStyle( 'core/separator', {
        name: 'small',
        label: 'Red Separator'
    } );

    wp.blocks.registerBlockStyle( 'core/separator', {
        name: 'small-centered',
        label: 'Red Separator Centered'
    } );

    wp.blocks.registerBlockStyle( 'core/columns', {
        name: 'center',
        label: 'Vertical Center'
    } );

    wp.blocks.registerBlockStyle( 'core/column', {
        name: 'card',
        label: 'Card Style'
    } );

    wp.blocks.registerBlockStyle( 'core/media-text', {
        name: 'card',
        label: 'Card Style'
    } );

    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'orange',
        label: 'Button Orange'
    } );

} );

