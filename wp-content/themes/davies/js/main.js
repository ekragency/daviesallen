jQuery(function ($) {
	$(document).ready(function () {

		$(document).on('click', 'a[href^="#"]', function (event) {
			event.preventDefault();
		
			$('html, body').animate({
				scrollTop: $($.attr(this, 'href')).offset().top
			}, 500);
		});
		
		$('.card-slider__slick').slick({
			adaptiveHeight: true,
			dots: true,
			slidesToShow: 1,
			prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-left"></i></button>',
			nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-right"></i></button>',
			autoplay: true,
			autoplaySpeed: 4000,
		});

		$('.image-slider__slick').slick({
			slidesToShow: 1,
			dots: false,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 3000,
			fade: true,
			cssEase: 'linear'
		});
		
		$('.team-slider__gallery').slick({
			slidesToShow: 1,
			dots: false,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 3000,
			fade: true,
			cssEase: 'linear'
		});
	})

});