<?php get_header(); ?>

<?php 
    $gallery = get_field('gallery');
?>

<main>
    <?php while(have_posts()) : the_post(); ?>
        <section class="person">
            <div class="uk-container">
                <div class="uk-flex uk-flex-middle">
                    <div class="card-slider__image">
                        <?php if($gallery) : ?>
                            <div class="card-slider__slick">
                                <?php foreach( $gallery as $image ) : ?>
                                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                                <?php endforeach; ?>
                            </div>
                        <?php else : ?>
                            <img src="<?= get_the_post_thumbnail_url(); ?>">
                        <?php endif; ?>
                    </div>
                    <div class="card-slider__content">
                        <h3><?= get_the_title(); ?></h3>
                        <p class="title"><?= get_field('company_position'); ?></p>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </section>
	<?php endwhile; ?>
</main>

<?php get_footer(); ?>