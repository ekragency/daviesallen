<?php get_header(); ?>

<main>
	<section class="error-page">
        <div class="uk-container uk-text-center">
            <h2>Error 404</h2>
            <p>The page you are looking for no longer exists or is missing.</p>
            <a href="<?= bloginfo('url') ?>" class="btn yellow">Back Home</a>
        </div>
    </section>
</main>

<?php get_footer(); ?>