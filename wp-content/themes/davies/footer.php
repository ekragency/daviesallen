<footer class="footer">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-expand@s">
                <a href="<?= bloginfo('url') ?>">
                    <img src="<?= get_template_directory_uri() ?>/img/logo.svg" alt="JA Logo">
                </a>
                <ul class="social">
                    <li><a href="<?= get_field('instagram','option') ?>"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="<?= get_field('facebook','option') ?>"><i class="fab fa-facebook"></i></a></li>
                    <li><a href="<?= get_field('twitter','option') ?>"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="<?= get_field('linkedin','option') ?>"><i class="fab fa-linkedin"></i></a></li>
                </ul>
                <?= wp_nav_menu(array('menu'=>'Footer Menu')) ?>
            </div>
            <div class="uk-width-expand">
                <h3><a href="<?= bloginfo('url') ?>/blog">Blog</a></h3>
                <div class="recent-post">
                <?php
                    $recent_posts = wp_get_recent_posts(array(
                        'numberposts' => 1, // Number of recent posts thumbnails to display
                        'post_status' => 'publish' // Show only the published posts
                    ));
                ?>
                <?php foreach($recent_posts as $post) : ?>
                    <a href="<?php echo get_permalink($post['ID']) ?>">
                        <p class="slider-caption-class"><?php echo $post['post_title'] ?></p>
                        <a href="<?= get_the_permalink($post['ID']) ?>">Read more</a>
                    </a>
                <?php endforeach; ?>
                </div>         
            </div>
            <div class="uk-width-1-2@m">
                <h3>Offices</h3>
                <div class="offices">
                    <?php while(have_rows('offices','option')) : the_row(); ?>
                        <div class="office">
                            <?= get_sub_field('office_info') ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

</body>

</html>