<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <div class="inner">
            <?php if( get_the_post_thumbnail_url() ) : ?>
            <div uk-grid>
                <div class="uk-width-1-3@s">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <div class="bg" style="background: url(<?= get_the_post_thumbnail_url(); ?>) no-repeat center top / cover;"></div>
                    </a>
                </div>
                <div class="uk-width-expand@s">
                    <div class="post__excerpt">
                        <span class="date"><?php the_time('j M Y'); ?></span>
                        <h4>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                        </h4>
                        <p><?php echo wp_trim_words(get_the_excerpt(), 18, ' [...]'); ?></p>
                        <a class="perm" href="<?= get_the_permalink(); ?>">Read More <i class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <?php else : ?>
            <div class="post__excerpt">
                <span class="date"><?php the_time('j M Y'); ?></span>
                <h4>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                </h4>
                <p><?php echo wp_trim_words(get_the_excerpt(), 18, ' [...]'); ?></p>
                <a class="perm" href="<?= get_the_permalink(); ?>">Read More <i class="fas fa-angle-right"></i></a>
            </div>
            <?php endif; ?>

        </div>

    </article>

<?php endwhile; ?>

<?php else: ?>

    <article>
        <div class="uk-text-center">
            <h3><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h3>
        </div>
    </article>

<?php endif; ?>
