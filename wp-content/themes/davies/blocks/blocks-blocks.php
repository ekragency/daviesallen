<?php /* Block Name: Blocks */ ?>  

<section id="<?= $id ?>" class="blocks">
    <div class="wp-block-columns">
        <?php while(have_rows('blocks')) : the_row(); ?>
            <?php 
                $title = get_sub_field('title');
                $content = get_sub_field('content');
            ?>
            <div class="wp-block-column is-style-card">
                <h4><?= $title ?></h4>
                <div class="toggle">
                    <?= $content ?>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
    <a href="#" class="btn orange" uk-toggle="target:.toggle;">Read More</a>
</section>