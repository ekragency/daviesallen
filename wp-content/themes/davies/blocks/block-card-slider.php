<?php /* Block Name: Card Slider */ ?>
<?php //variables
    $type = get_field('card_slider_type');
    $people = get_field('people');
    $id = 'card-slider-' . $block['id'];
?>

<section id="<?= $id ?>" class="card-slider">
    <div class="uk-container">
        <?php if($type == 'slider') : ?>
        <div class="card-slider__slick">
            <?php foreach( $people as $person) : ?>
                <div>
                    <div class="card-slider__person">
                        <div class="card-slider__inner">
                            <div class="uk-flex uk-flex-middle">
                                <?php if(has_post_thumbnail($person->ID)) : ?>
                                <div class="card-slider__image">
                                    <div style="background: url(<?= get_the_post_thumbnail_url($person->ID, 'large'); ?>) no-repeat center center / cover; width: 100%; height: 340px;"></div>
                                </div>
                                <?php endif; ?>
                                <div class="card-slider__content">
                                    <h3><?= get_the_title($person->ID); ?></h3>
                                    <p class="title"><?= get_field('company_position', $person->ID); ?></p>
                                    <?= $person->post_excerpt; ?>
                                    <div class="wp-block-buttons">
                                        <div class="wp-block-button"><a class="wp-block-button__link" href="<?= get_the_permalink($person->ID) ?>">Read More</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>

        <?php if($type == 'grid') : ?>
        <div class="card-slider__grid">
            <div class="uk-child-width-1-4@m uk-child-width-1-2 uk-grid-collapse uk-grid-match" uk-grid>
                <?php foreach( $people as $person) : ?>
                <div class="card-slider__person">
                    <div class="card-slider__inner">
                        <?php if(has_post_thumbnail($person->ID)) : ?>
                        <div class="card-slider__image">
                            <div style="background: url(<?= get_the_post_thumbnail_url($person->ID, 'large'); ?>) no-repeat center center / cover; width: 100%; height: 200px;"></div>
                        </div>
                        <?php endif; ?>
                        <div class="card-slider__content">
                            <h3><?= get_the_title($person->ID); ?></h3>
                            <p class="title"><?= get_field('company_position', $person->ID); ?></p>
                            <?= $person->post_excerpt; ?>
                            <div class="wp-block-buttons">
                                <div class="wp-block-button"><a class="wp-block-button__link" href="<?= get_the_permalink($person->ID) ?>">Read More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if(get_field('page_link') || get_field('link_label')) : ?>
        <div class="card-slider__link">
            <a class="btn orange" href="<?= get_field('page_link') ?>"><?= get_field('link_label'); ?></a>
        </div>
        <?php endif; ?>
    </div>
</section>