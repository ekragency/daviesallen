<?php /* Block Name: Blocks */ ?>  

<section id="<?= $id ?>" class="blocks">
    <div class="wp-block-columns">
        <?php while(have_rows('blocks')) : the_row(); ?>
            <?php 
                $title = get_sub_field('title');
                $content = get_sub_field('content');
            ?>
            <div class="wp-block-column is-style-card">
                <h4><?= $title ?></h4>
                <div class="toggle" hidden>
                    <?= $content ?>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
    <div class="uk-text-center">
        <a href="#" class="btn orange toggle" uk-toggle="target:.toggle; animation: uk-animation-fade">Read More</a>
        <a hidden href="#" class="btn orange toggle" uk-toggle="target:.toggle; animation: uk-animation-fade">Read Less</a>
    </div>
</section>