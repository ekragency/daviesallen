<?php /* Block Name: Image Slider */ ?>
<?php //variables
    $images = get_field('images');
?>

<section id="<?= $id ?>" class="image-slider">
    <div class="image-slider__slick">
        <?php foreach( $images as $image ) : ?>
            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
        <?php endforeach; ?>
    </div>
</section>