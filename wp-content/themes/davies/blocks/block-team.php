<?php /* Block Name: Team Block */ ?>
<?php //variables
    $people = get_field('people');
?>

<section id="<?= $id ?>" class="team-slider">
    <div class="uk-container">
        <div class="team-slider uk-child-width-1-5@m uk-child-width-1-2" uk-grid>
            <?php foreach( $people as $person) : ?>
            <?php 
                    $personid = $person->ID; 
                    $gallery = get_field('gallery', $personid);
                ?>
            <div>
                <div class="team-slider__person">
                    <div class="team-slider__inner">
                        <?php if(count($gallery) > 1) : ?>
                        <div class="team-slider__gallery">
                            <?php foreach( $gallery as $image ) : ?>
                            <div>
                                <div
                                    style="background: url(<?= $image['url'] ?>) no-repeat center center / cover; width: 200px; height: 200px;">
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <?php else : ?>
                        <div class="team-slider__image">
                            <div
                                style="background: url(<?= get_the_post_thumbnail_url($personid, 'large'); ?>) no-repeat center center / cover; width: 200px; height: 200px;">
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="team-slider__content">
                            <h5><?= get_the_title($personid); ?></h5>
                            <p class="title"><?= get_field('company_position', $personid); ?></p>
                            <div class="wp-block-buttons">
                                <div class="wp-block-button">
                                    <a href="#" class="wp-block-button__link"
                                        uk-toggle="target: #model-<?= $personid ?>">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="model-<?= $personid ?>" class="uk-flex-top" uk-modal>
                    <div class="uk-modal-dialog uk-margin-auto-vertical">
                        <button class="uk-modal-close-outside" type="button" uk-close></button>
                        <div class="uk-flex uk-flex-middle">
                            <div class="card-slider__image">
                                <div
                                    style="background: url(<?= get_the_post_thumbnail_url($personid, 'large'); ?>) no-repeat center center / cover; width: 100%; height: 400px">
                                </div>
                            </div>
                            <div class="card-slider__content">
                                <h3><?= get_the_title($personid); ?></h3>
                                <p class="title"><?= get_field('company_position', $personid); ?></p>
								<?php if($person->post_content): ?>
									<?= $person->post_content; ?>
								<?php else: ?>
									<?= $person->post_excerpt; ?>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>