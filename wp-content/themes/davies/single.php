<?php get_header('default'); ?>

<main>
	<?php while(have_posts()) : the_post(); ?>
		<div class="single-post">
			<section class="banner-image uk-text-center">
				<img src="<?= get_the_post_thumbnail_url(); ?>" alt="">
			</section>
			<section class="intro">
				<div class="uk-container uk-container-small">
					<p class="uk-flex uk-flex-center meta">
						<span><?= get_the_date(); ?> </span>
						<span> | By <?= get_the_author(); ?></span>
					</p>
					<h2><?= get_the_title(); ?></h2>
				</div>
			</section>
			<section class="post__content">
				<div class="uk-container uk-container-small">
					<?php the_content(); ?>
				</div>
			</section>
		</div>
	<?php endwhile; ?>
</main>

<?php get_footer('default'); ?>